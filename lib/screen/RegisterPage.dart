import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:flutter/gestures.dart';
import 'package:myggguestlogbook/screen/HomePage.dart';
import 'package:myggguestlogbook/screen/VisitPage.dart';
import 'package:myggguestlogbook/validation/valRegistration.dart';
import 'package:myggguestlogbook/tools/dialogBox.dart';
import 'package:myggguestlogbook/service/svcGuest.dart';
import 'package:myggguestlogbook/model/CGuest.dart';
import 'package:myggguestlogbook/model/CSite.dart';
import 'package:myggguestlogbook/model/CLookup.dart';
import 'package:myggguestlogbook/model/CQuestionaire.dart';
import 'package:myggguestlogbook/service/svcLookup.dart';
import 'package:myggguestlogbook/service/svcQuestionaire.dart';
import 'package:myggguestlogbook/validation/valPSK.dart';
import 'package:easy_localization/easy_localization.dart';



class RegisterPage extends StatefulWidget {
//  final String siteparam;
  final CSite objSite;
  final String noKtp;

  /*
  RegisterPage({Key key,
      @required this.siteparam,
      @required this.noKtp}) : super(key: key);
  */
  RegisterPage({Key key,
    @required this.objSite,
    @required this.noKtp}) : super(key: key);

  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> with valRegistration {
  Scaffold stagger;
  bool isSwitched = false;

  final dialogBox message           = dialogBox();
  final valPSK validationPSK        = valPSK();

  final scaffoldStateKey = GlobalKey<ScaffoldState>();
  final formRegisterKey  = GlobalKey<FormState>(); // globalkey for validation

  FocusNode NodeNama        = FocusNode();
  FocusNode NodeNip         = FocusNode();
  FocusNode NodePhoneno     = FocusNode();
  FocusNode NodeEmail       = FocusNode();
  FocusNode NodeCompany     = FocusNode();
  FocusNode NodeSwitch      = FocusNode();
  FocusNode NodeCmdRegister = FocusNode();
  FocusNode NodeCmdCancel   = FocusNode();
  FocusNode NodePSKey       = FocusNode();

  final TextEditingController _ctrlName    = TextEditingController();
  final TextEditingController _ctrlNip     = TextEditingController();
  final TextEditingController _ctrlPhoneno = TextEditingController();
  final TextEditingController _ctrlEmail   = TextEditingController();
  final TextEditingController _ctrlCompany = TextEditingController();
  final TextEditingController _ctrlPSKey   = TextEditingController();

  String strName    = '';
  String strNip     = '';
  String strPhoneno = '';
  String strEmail   = '';
  String strCompany = '';
  String strPSKey   = '';

  bool _isLoading = false;
  bool _pskshow = false;

  Future<void> _callInitServiceVisit(BuildContext context, CGuest objGuest, CSite objSite) async{
    List<CLookup> listDocAssurance;
    List<CQuestionaire> listQuestionaire;

    svcLookup lookupService = new svcLookup();
    svcQuestionaire questionaireService = new svcQuestionaire();

    await lookupService.getListLookupByGroup('AssuranceCard')
        .then((List<CLookup> objResultx) {
      listDocAssurance = objResultx;
      // reload questionaire site

      print("objSite.siteId           ->" + objSite.siteId.toString());
      print("objSite.formaccessmentid ->" + objSite.formaccessmentid.toString());

      // with questionaire
      if(objSite.formaccessmentid > 0) {
        print('using que');
        questionaireService.getListQuestionaireById(
            objSite.formaccessmentid.toString(),
            context.locale.toString())
            .then((List<CQuestionaire> objResulty) {
          listQuestionaire = objResulty;
          // jika belum ada jadwal kunjungan maka redirect ke Visit Page
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) =>
                  VisitPage(objSite: objSite,
                      objGuest: objGuest,
                      objDoc: listDocAssurance,
                      objQue: listQuestionaire
                  )), (route) => false);
        }).catchError((e){ });
      }
      else{
        print('not using que');
        // without questionaire just redirect
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) =>
                VisitPage(objSite: objSite,
                    objGuest: objGuest,
                    objDoc: listDocAssurance,
                    objQue: listQuestionaire
                )), (route) => false);
      }
    }).catchError((e){
      print('Error -> ' + e.toString());
    });
  }

  Future<CGuest> _callServiceRegister(BuildContext context, CGuest objGuest) async{
    CGuest result;
    svcGuest guestService = new svcGuest();
    await guestService.setRegisterGuest(objGuest)
        .then((CGuest objResult) {

          // update : 2022-06-13 : after registration direct no need to login again :
          // redirect to home page
          // Navigator.of(context).pushAndRemoveUntil(
          //     MaterialPageRoute(builder: (context) => HomePage(objSite: widget.objSite)), (
          //     route) => false);

          // direct to visit page
          _callInitServiceVisit(context, objResult, widget.objSite);

          //Navigator.pop(context);
      result = objResult;
    })
        .catchError((e){
    });
    return result;
  }

  void EH_Cmd_Register(BuildContext context) {

    var privacytitle      = tr('RegisterPage.EH_Cmd_Register->privacytitle');
    var privacymessage    = tr('RegisterPage.EH_Cmd_Register->privacymessage');

      if(isSwitched){
        if (!_isLoading) {
          // check trough validation first
          if (formRegisterKey.currentState.validate()) {
            formRegisterKey.currentState.save();
            // init loading to true
            setState(() { _isLoading = true; });
            try {
              // save info in regis page1 into class
              _callServiceRegister(
                  context,
                  CGuest(
                    guestId: 0,
                    //widget.siteparam.toString(),
                    guestCardId: strNip,
                    guestName: strName,
                    guestPhone: strPhoneno,
                    guestEmail: strEmail,
                    guestCompany: strCompany,
                    isActive: 'Y'
                )
              );
            }
            catch(e){
              print(e);
            }
            // set loading to false
            setState(() { _isLoading = false; });
          } // end if
        } // end if
        else{
          // clear progress dialog
          Navigator.pop(context);
        }
      }
      else{
        //message.showMessageDialog(context, "Privacy Statement","Please Accept The Privacy Statement");
        message.showMessageDialog(context, '$privacytitle','$privacymessage');

      }
  }
  Widget Lbl_Welcome(BuildContext context) {
    var welcomeMessage   = tr('RegisterPage.Lbl_Welcome->welcomeMessage');
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          // new TextSpan(text: 'Daftarkan data diri anda terlebih dahulu disini')
          new TextSpan(text: '$welcomeMessage')
        ],
      ),
    );
  }
  Widget Img_Logo(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(8.0),
        topRight: Radius.circular(8.0),
      ),
      child: Image.asset('assets/images/register.png',
          width: 300,
          height: 120,
          fit:BoxFit.fill
      ),
    );
  }

  Widget Txt_Nama(BuildContext context) {
    var label   = tr('RegisterPage.Txt_Nama->label');
    return TextFormField(
      controller: _ctrlName,
      focusNode: NodeNama,
      keyboardType: TextInputType.text,
      validator: validateName,
      onSaved: (String value) {
        strName = value;
      },
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.person),
        // labelText: 'Full Name',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePhoneno);
      },
    );
  }
  Widget Txt_Nip(BuildContext context) {
    var label   = tr('RegisterPage.Txt_Nip->label');
    return TextFormField(
      enabled: false,
      readOnly: true,
      controller: _ctrlNip,
      focusNode: NodeNip,
      validator: validateNip,
      onSaved: (String value) {
        strNip = value;
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.perm_identity),
        //labelText: 'No KTP',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePhoneno);
      },
    );
  }
  Widget Txt_PhoneNo(BuildContext context) {
    var label   = tr('RegisterPage.Txt_PhoneNo->label');
    return TextFormField(
      controller: _ctrlPhoneno,
      focusNode: NodePhoneno,
      validator: validatePhoneNo,
      onSaved: (String value) {
        strPhoneno = value;
      },
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly
      ],
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.phone),
        // labelText: 'Mobile Phone Number',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeEmail);
      },
    );
  }
  Widget Txt_Email(BuildContext context) {
    var label   = tr('RegisterPage.Txt_Email->label');
    return TextFormField(
      controller: _ctrlEmail,
      focusNode: NodeEmail,
      validator: validateEmail,
      onSaved: (String value) {
        strEmail = value;
      },
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.email),
        // labelText: 'Email',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCompany);
      },
    );
  }
  Widget Txt_Company(BuildContext context) {
    var label   = tr('RegisterPage.Txt_Company->label');
    return TextFormField(
      controller: _ctrlCompany,
      focusNode: NodeCompany,
      validator: validateCompany,
      onSaved: (String value) {
        strCompany = value;
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.business),
        // labelText: 'Company Name',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeSwitch);
      },
    );
  }

  Widget Switch_Agreement(BuildContext context){
    var label             = tr('RegisterPage.Switch_Agreement->label');
    var privacylabel      = tr('RegisterPage.Switch_Agreement->privacylabel');
    var privacytitle      = tr('RegisterPage.Switch_Agreement->privacytitle');
    var privacystatement  = tr('RegisterPage.Switch_Agreement->privacystatement');

    //var msg = 'We collect this data in the context of\n• Notify your host of your arrival\n• Security and internal  audits\n• In case of emergency situations\n• Draw up visitor statistics\n\nUse of personal data\n• To notify the host of your arrival\n• To notify tyoy in case of emergency\n\nDisclosure to third parties\n• We do not sell or share any of your personal\ninformation to others parties';

    TextStyle defaultStyle = TextStyle(color: Colors.grey, fontSize: 20.0);
    TextStyle linkStyle = TextStyle(
      color: Colors.blue,
      fontSize: 18.0,
      fontStyle: FontStyle.italic,
    );
    return Row(
        children : <Widget> [
          Switch(
            focusNode: NodeSwitch,
            value: isSwitched,
            onChanged: (value){
              FocusScope.of(context).requestFocus(NodeCmdRegister);
              setState(() {
                isSwitched = value;
                _pskshow = false;

                // change 20220614: no need to show auth key.
                // display psk key
                // if(isSwitched == true){
                //   _pskshow = true;
                // }
                // else{
                //   _pskshow = false;
                // }

                //print(isSwitched);
              });
            },
            activeTrackColor: Colors.black,
            activeColor: Colors.white,
          ),
          RichText(
            text: TextSpan(
                style: defaultStyle,
                children: <TextSpan>[
                  // TextSpan(text: 'I agree with '),
                  TextSpan(text: '$label'),
                  // TextSpan(text: 'the privacy statement',
                  TextSpan(text: '$privacylabel',
                      style: linkStyle,
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          //showAgreementDialog(context);
                          // message.showMessageDialog(context, "Privacy Statement",
                          //     'We collect this data in the context of\n' +
                          //         '• Notify your host of your arrival\n' +
                          //         '• Security and internal  audits\n' +
                          //         '• In case of emergency situations\n' +
                          //         '• Draw up visitor statistics\n\n' +
                          //         'Use of personal data\n' +
                          //         '• To notify the host of your arrival\n' +
                          //         '• To notify tyoy in case of emergency\n\n' +
                          //         'Disclosure to third parties\n' +
                          //         '• We do not sell or share any of your personal\n' +
                          //         'information to others parties');

                          message.showMessageDialog(context,'$privacytitle','$privacystatement');
                        }),
                ]
            ),
          )
        ]
    );




  }

  Widget Txt_PSKey(BuildContext context) {
    var label   = tr('RegisterPage.Txt_PSKey->label');
    return TextFormField(
      controller: _ctrlPSKey,
      focusNode: NodePSKey,
      keyboardType: TextInputType.text,
      validator: (value) {
        return validationPSK.validatePSK(value, widget.objSite);
      },
      onSaved: (String value) {
        strPSKey = value;
      },
      autofocus: false,
      decoration: InputDecoration(
        //icon: const Icon(Icons.vpn_key),
        // labelText: 'Authentication Key',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdRegister);
      },
    );
  }

  Widget Cmd_Register(BuildContext context) {
    var label   = tr('RegisterPage.Cmd_Register->label');
    return InkWell(
        focusNode: NodeCmdRegister,
        onTap: () => EH_Cmd_Register(context),
        child: new Container(
          width: 200.0,
          height: 50.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.login),
                //Text('Submit',style: TextStyle(fontSize: 16.0, color: Colors.black ))
                Text('$label',style: TextStyle(fontSize: 16.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    var label   = tr('RegisterPage.Cmd_Cancel->label');
    return InkWell(
        focusNode: NodeCmdCancel,
        onTap: () {
          /*
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (_) {
                return HomePage(siteparam: widget.siteparam);
              })
          );
          */
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (_) {
                return HomePage(objSite: widget.objSite);
              })
          );

        },
        child: new Container(
          width: 200.0,
          height: 50.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Text('Cancel', style: TextStyle(fontSize: 16.0, color: Colors.black ))
                Text('$label', style: TextStyle(fontSize: 16.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget form_Register(BuildContext context) {
    return Column(
      children: [
            SizedBox(height: 10.0),
            Lbl_Welcome(context),
            SizedBox(height: 20.0),
            Form(
                key: formRegisterKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Txt_Nama(context),
                    SizedBox(height: 10.0),
                    Txt_Nip(context),
                    SizedBox(height: 10.0),
                    Txt_PhoneNo(context),
                    SizedBox(height: 10.0),
                    Txt_Email(context),
                    SizedBox(height: 10.0),
                    Txt_Company(context),
                    SizedBox(height: 10.0),
                    Switch_Agreement(context),
                    SizedBox(height: 10.0),
                    _pskshow == true ?
                    Padding(
                        child: Txt_PSKey(context),
                        padding: EdgeInsets.only(left: 80, right:80)
                    ) : Column(),
                    SizedBox(height: 10.0),
                    Cmd_Register(context),
                    Cmd_Cancel(context)
                  ],
                )
            )
      ],
    );
  }

  @override
  void initState() {
    _ctrlNip.text = widget.noKtp;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => svcGuest(),
        )
      ],
      child: MaterialApp(
          home: new Scaffold(
              appBar: new AppBar(
                backgroundColor: Colors.white,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/logo.png', fit: BoxFit.cover, height: 40.0,),
                  ],),
              ),
              body:
              Center(
                  child: Container(
                    margin: const EdgeInsets.all(10.0),
                    color: Colors.transparent,
                    width: 500.0,
                    child: ListView(
                      shrinkWrap: false,
                      padding: EdgeInsets.only(left: 24.0, right: 24.0),
                      children: <Widget>[
                        form_Register(context)
                      ],
                    ),
                  )
              )
          )
      ),
    );

  }
}