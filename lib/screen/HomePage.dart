import 'package:flutter/material.dart';
import 'package:myggguestlogbook/model/CLanguage.dart';
import 'package:myggguestlogbook/validation/valCekKTP.dart';
import 'package:myggguestlogbook/model/CGuest.dart';
import 'package:myggguestlogbook/model/CSite.dart';
import 'package:myggguestlogbook/model/CVisit.dart';
import 'package:myggguestlogbook/model/CVisitSaveReq.dart';
import 'package:myggguestlogbook/model/CLookup.dart';
import 'package:myggguestlogbook/model/CQuestionaire.dart';
import 'package:myggguestlogbook/model/CLanguage.dart';
import 'package:myggguestlogbook/service/svcGuest.dart';
import 'package:myggguestlogbook/service/svcVisit.dart';
import 'package:myggguestlogbook/service/svcLookup.dart';
import 'package:myggguestlogbook/service/svcQuestionaire.dart';
import 'package:myggguestlogbook/screen/RegisterPage.dart';
import 'package:myggguestlogbook/screen/VisitPage.dart';
import 'package:myggguestlogbook/screen/SessionPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:myggguestlogbook/tools/slidercaptcha/SliderCaptcha.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';

class HomePage extends StatefulWidget {
  //final String siteparam;
  final CSite objSite;

  //HomePage({Key key, @required this.siteparam}) : super(key: key);
  HomePage({Key key, @required this.objSite}) : super(key: key);

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> with valCekKTP {

  final formInputKTPKey  = GlobalKey<FormState>();

  FocusNode NodeNoKTP           = FocusNode();
  FocusNode NodeCmdCheckKTP     = FocusNode();
  FocusNode NodeCmdCaptcha      = FocusNode();

  TextEditingController _ctrlNoKTP  = new TextEditingController();

  String strNoKTP;
  String resultPage;
  List<CLanguage> listLanguage;
  CLanguage _selectedLanguage;

  Future<void> _callInitServiceVisit(BuildContext context, CGuest objGuest, CSite objSite) async{
    List<CLookup> listDocAssurance;
    List<CQuestionaire> listQuestionaire;

    svcLookup lookupService = new svcLookup();
    svcQuestionaire questionaireService = new svcQuestionaire();

    await lookupService.getListLookupByGroup('AssuranceCard')
        .then((List<CLookup> objResultx) {
            listDocAssurance = objResultx;
            // reload questionaire site

            print("objSite.siteId           ->" + objSite.siteId.toString());
            print("objSite.formaccessmentid ->" + objSite.formaccessmentid.toString());
            // with questionaire
            if(objSite.formaccessmentid > 0) {
              print('using que');
              questionaireService.getListQuestionaireById(
                  objSite.formaccessmentid.toString(),
                  context.locale.toString())
                  .then((List<CQuestionaire> objResulty) {
                       listQuestionaire = objResulty;
                      // jika belum ada jadwal kunjungan maka redirect ke Visit Page
                       Navigator.of(context).pushAndRemoveUntil(
                           MaterialPageRoute(builder: (context) =>
                               VisitPage(objSite: objSite,
                                   objGuest: objGuest,
                                   objDoc: listDocAssurance,
                                   objQue: listQuestionaire
                               )), (route) => false);
                  }).catchError((e){ });
            }
            else{
              print('not using que');
              // without questionaire just redirect
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) =>
                      VisitPage(objSite: objSite,
                          objGuest: objGuest,
                          objDoc: listDocAssurance,
                          objQue: listQuestionaire
                      )), (route) => false);
            }
    }).catchError((e){
          print('Error -> ' + e.toString());
    });
  }

  Future <void> do_check_session(CSite objSite) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      try{
        // check from preference if its has session_id
        //preferences.setString("siteCode", objVisit.objSite.siteCode);
        //preferences.setString("NoKTP", objVisit.objGuest.guestCardId);
        //print('shared pref -> ' + preferences.getString("NoKTP"));

        if(preferences.getString("NoKTP") != null) {
          _ctrlNoKTP.text = preferences.getString("NoKTP");
          _callServiceCheckKTP(context, objSite, preferences.getString("NoKTP"));
        }
        else{
          //print('No Session Saved');
        }
      }
      catch(exception){
      }
    });
  }

  Future<CVisit> _callServiceCheckActiveVisitByKTP(BuildContext context, CGuest objGuest, CSite objSite, String strNoKTP) async{
    CVisit result;
    svcVisit visitService = new svcVisit();
    await visitService.getActiveVisitbyguestCardId(strNoKTP)
        .then((CVisit objVisit) {
              if(objVisit.visitId > 0){
                // kalo session visit masih active maka redirect ke session page
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) {
                      return SessionPage(
                          objSite:  objSite, //pake site dimana dia pakai barcode site,
                          objGuest:  objVisit.objGuest, //objGuest,
                          objContact: objVisit.objContact, //  objSelectedContact,
                          objVisitReq: CVisitSaveReq(
                                siteid: objSite.siteId, //widget.objSite.siteId,
                                guestid: objVisit.objGuest.guestId,     //widget.objGuest.guestId,
                                contactid: objVisit.objContact.contactId,   // objSelectedContact.contactId,
                                visitid: objVisit.visitId,
                                visitpurpose: objVisit.visitPurpose,  //strVisitPurpose,
                                contactaltname: null  //objVisit.objContact.contactAltName
                                // (checkNotListed == true) ? strVisitContactName : null
                          )
                      );
                    }));
              }
              else{
                _callInitServiceVisit(context, objGuest, objSite);
              }
              result = objVisit;
        }).catchError((e){ });

    return result;
  }

  Future<CGuest> _callServiceCheckKTP(BuildContext context, CSite objSite, String strNoKTP) async{
    CGuest result;
    svcGuest guestService = new svcGuest();
    await guestService.getGuestByKTP(strNoKTP)
        .then((CGuest objResult) {
              result = objResult;
              if(objResult.guestId == -1){
                // kalo belum terdaftar maka redirect ke Register Page
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) =>
                        RegisterPage(
                            objSite: objSite, //widget.objSite,
                            noKtp: strNoKTP
                        )), (route) => false);
              }
              else{
                // kalo sudah terdaftar maka lanjutkan check di session visit
                _callServiceCheckActiveVisitByKTP(context, objResult, objSite, strNoKTP);
              }
         }).catchError((e){ });
    return result;
  }

  Widget ProgressIndicator(String strTitle){
     return //CircularProgressIndicator();
      Container(
          padding: EdgeInsets.all(16),
          color: Colors.black.withOpacity(0.8),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                    child: Container(
                        child: CircularProgressIndicator(
                            strokeWidth: 3
                        ),
                        width: 32,
                        height: 32
                    ),
                    padding: EdgeInsets.only(bottom: 16)
                ),
                Text(strTitle,
                  style: TextStyle(color: Colors.white, fontSize: 12),
                  textAlign: TextAlign.center,
                )
              ]
          )
      );
  }
  Widget Img_Avatar(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(8.0),
        topRight: Radius.circular(8.0),
      ),
      child: Image.asset('assets/images/avatar.png',
          //width: 200,
          //height: 100,
          width: MediaQuery.of(context).size.width >= 500 ? 200.0 : 160.0,
          height: MediaQuery.of(context).size.width >= 500 ? 100.0 : 80.0,
          fit:BoxFit.fill
      ),
    );
  }
  Widget Lbl_Welcome1(BuildContext context, CSite objSite) {
    //var welcomeMessage = await translator.translate("Selamat Datang", from: 'in', to: 'en');
    var welcomeMessage   = tr('HomePage.Lbl_Welcome1->welcomeMessage');
    var welcomeToCompany = tr('HomePage.Lbl_Welcome1->welcomeToCompany');
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          //fontSize: 20.0,
          fontSize: MediaQuery.of(context).size.width >= 500 ? 20.0 : 18.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          // new TextSpan(text: 'Selamat Datang\n', style: new TextStyle(fontWeight: FontWeight.bold)),t
          // new TextSpan(text: 'di PT Gudang Garam Tbk.\n'),
          new TextSpan(text: '$welcomeMessage', style: new TextStyle(fontWeight: FontWeight.bold)),
          new TextSpan(text: '$welcomeToCompany'),
          //new TextSpan(text: 'Site \n'+ widget.siteparam.toString())
          new TextSpan(text: 'Site : '+ objSite.siteName)
        ],
      ),
    );
  }
  Widget Lbl_Welcome2(BuildContext context) {
    var instructionMessage = tr('HomePage.Lbl_Welcome2->instructionMessage');
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          //fontSize: 20.0,
          fontSize: MediaQuery.of(context).size.width >= 500 ? 20.0 : 18.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          // new TextSpan(text: 'Silahkan cek No KTP / Paspor anda dibawah'),
           new TextSpan(text: '$instructionMessage'),
        ],
      ),
    );
  }
  Widget Txt_NoKTP(BuildContext context) {
    var lblTxtNoKTP = tr("HomePage.Txt_NoKTP->label");
    return TextFormField(
      controller: _ctrlNoKTP,
      focusNode: NodeNoKTP,
      keyboardType: TextInputType.text,
      validator: validateNoKTP,
      onSaved: (String value) {
        strNoKTP = value;
      },
      style: TextStyle(
          //fontSize: 16.0,
          fontSize: MediaQuery.of(context).size.width >= 500 ? 16.0 : 14.0,
          color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.document_scanner_outlined),
        //labelText: 'No KTP/ No Paspor'
        labelText: '$lblTxtNoKTP'
      ),
      onFieldSubmitted: (_) {
          FocusScope.of(context).requestFocus(NodeCmdCheckKTP);
      },
    );
  }
  Widget Cmd_CheckKTP(BuildContext context, CSite objSite) {
    var lblCmdCheckKTP = tr("HomePage.Cmd_CheckKTP->label");
    return InkWell(
        focusNode: NodeCmdCheckKTP,
        onTap: () {
          if (formInputKTPKey.currentState.validate()) {
            formInputKTPKey.currentState.save();
            print(strNoKTP);

            showCaptcha(context, objSite, strNoKTP);
            //getFutureSubmit(context, strNoKTP);
            //_callServiceCheckKTP(context, objSite, strNoKTP);
          }
        },
        child: new Container(
          width: 180.0,
          height: 30.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Text('Check Identitas', style: TextStyle(fontSize: 12.0, color: Colors.black ))
                Text('$lblCmdCheckKTP', style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cbo_LanguageSelect(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          // margin: EdgeInsets.only(top: 80.0, left: 28.0),
          margin: EdgeInsets.only(top: 20.0, left: 78.0),
          child: Icon(
            Icons.language,
            color: Colors.grey,
            size: 20.0,
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.transparent,
          ),
          padding: EdgeInsets.only(left: 50.0),
          //padding: EdgeInsets.only(left: 60.0),
          //margin: EdgeInsets.only(top: 64.0, left: 16.0, right: 16.0),
          margin: EdgeInsets.only(top: 4.0, left: 60.0, right: 78.0),
          child: DropdownButton(
            isExpanded: true,
            items: listLanguage.map((CLanguage language) {
                return DropdownMenuItem(
                  value: language,
                  child: Text(language.langName,
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black
                      )),
                );
              },
            ).toList(),
            value: _selectedLanguage,
            onChanged: (newLanguage) {
              print('language change -> ' + newLanguage.langCode);
              setState(() {
                  _selectedLanguage = newLanguage;
                  context.setLocale(_selectedLanguage.langCode.toLocale(separator: '_'));
              });
            },
          ),
        ),
      ],
    );
  }
  void showCaptcha(BuildContext context, CSite objSite, String strNoKTP) {
    var lblInstruction = tr("HomePage.showCaptcha->lblInstruction");
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: Container(
              width: 150, //MediaQuery.of(context).size.width *0.4, //double.infinity,
              height: 280, //MediaQuery.of(context).size.height*0.3,
              child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: SliderCaptcha(
                      image:  Image.asset('images/powerby.png',
                          height: 150,
                          width: 280,
                          fit: BoxFit.fitWidth
                      ),
                      instruction: '$lblInstruction',
                      onSuccess: (){
                        _callServiceCheckKTP(context, objSite, strNoKTP);
                      }
                  ),
              ),
            )
          );
        });
  }

  Widget form_welcome(BuildContext context, CSite objSite) {
    return Form(
        key: formInputKTPKey,
        child: Column(
          children: <Widget>[
            Lbl_Welcome1(context, objSite),
            SizedBox(height: MediaQuery.of(context).size.width >= 500 ? 20.0 : 10.0),
            Img_Avatar(context),
            Cbo_LanguageSelect(context),
            SizedBox(height: MediaQuery.of(context).size.width >= 500 ? 20.0 : 10.0),
            Lbl_Welcome2(context),
            SizedBox(height: MediaQuery.of(context).size.width >= 500 ? 20.0 : 10.0),
            Txt_NoKTP(context),
            SizedBox(height: MediaQuery.of(context).size.width >= 500 ? 50.0 : 10.0),
            Cmd_CheckKTP(context, objSite)
          ],
        )
    );
  }


  @override
  void initState() {
    super.initState();
    // check shared preference if exist then checked
    //print("widget.objSite.siteCode -> " + widget.objSite.siteCode);
    //print("widget.objSite.formaccessmentid -> " + widget.objSite.formaccessmentid.toString());

    // init language code
    setState(() {
      listLanguage = [
        new CLanguage(langCode: 'en_US', langName: 'English'),
        new CLanguage(langCode: 'id_ID', langName: 'Indonesia')
      ];
    });
    do_check_session(widget.objSite);
  }

  @override
  Widget build(BuildContext context) {
    // load and set default selected language based on profile
    setState(() {
      print('Current Local -> ' + context.locale.toString());
      _selectedLanguage = listLanguage.firstWhere((language) => language.langCode.contains(context.locale.toString()));
    });

    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset('assets/images/logo.png',fit: BoxFit.cover, height: 40.0,),
            ],),
        ),
        body:
        Center(
            child: Container(
              margin: const EdgeInsets.all(10.0),
              color: Colors.transparent,
              width: 500.0,
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                    form_welcome(context, widget.objSite)
                ],
              ),
            )
        )
    );
  }
}