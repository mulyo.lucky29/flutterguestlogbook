import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:myggguestlogbook/screen/HomePage.dart';
import 'package:myggguestlogbook/screen/NotFoundPage.dart';
import 'package:myggguestlogbook/model/CSite.dart';
import 'package:myggguestlogbook/service/svcSite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:easy_localization/easy_localization.dart';

class SplashPage extends StatefulWidget {
  final String siteparam;

  SplashPage({Key key, @required this.siteparam}) : super(key: key);

  @override
  _SplashPageState createState() => new _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with SingleTickerProviderStateMixin {
  var _visible = true;

  AnimationController animationController;
  Animation<double> animation;
  //  final sessionChecker sessionck      = sessionChecker();

  Future<void> _deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();
    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  Future<void> _deleteAppDir() async {
    final appDir = await getApplicationSupportDirectory();
    if(appDir.existsSync()){
      appDir.deleteSync(recursive: true);
    }
  }

  startTime() async {
    // add clear cache procedure
    _deleteCacheDir();
    _deleteAppDir();

    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    get_session(context);
  }

  Future <void> get_session(BuildContext context) async{
    //SharedPreferences preferences = await SharedPreferences.getInstance();
    //String vstdate;

    svcSite siteService = new svcSite();
    await siteService.getSiteInfoByCode(widget.siteparam)
        .then((CSite objResult) {
            if (objResult.siteId > 0) {
              // redirect to home page with site code
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (_) {
                    return HomePage(objSite: objResult);
                  }));
            }
            else{
              // redirect to not found page
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (_) {
                    return NotFoundPage();
                  }));
            }
    })
        .catchError((e){
    });
  }

  @override
  void initState() {
    super.initState();

    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 2),
    );
    animation = new CurvedAnimation(parent: animationController, curve: Curves.easeOut);
    animation.addListener(() => this.setState(() {}));
    animationController.forward();
    setState(() {
      _visible = !_visible;
    });
    startTime();
  }

  @override
  dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset('assets/images/logo.png',
                width: animation.value * 250,
                height: animation.value * 250,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
