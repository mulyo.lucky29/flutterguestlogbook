import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:achievement_view/achievement_view.dart';
import 'package:myggguestlogbook/screen/HomePage.dart';
import 'package:myggguestlogbook/screen/ListLogPage.dart';
import 'package:myggguestlogbook/model/CGuest.dart';
import 'package:myggguestlogbook/model/CSite.dart';
import 'package:myggguestlogbook/model/CVisit.dart';
import 'package:myggguestlogbook/model/CContact.dart';
import 'package:myggguestlogbook/model/CVisitSaveReq.dart';
import 'package:myggguestlogbook/model/CVisitEndReq.dart';
import 'package:myggguestlogbook/model/CCheckLog.dart';
import 'package:myggguestlogbook/model/CCheckLogReq.dart';
import 'package:myggguestlogbook/service/svcVisit.dart';
import 'package:myggguestlogbook/service/svcSite.dart';
import 'package:myggguestlogbook/service/svcCheckLog.dart';
import 'package:easy_localization/easy_localization.dart';

class SessionPage extends StatefulWidget {
  final CSite objSite;
  final CGuest objGuest;
  final CContact objContact;
  final CVisitSaveReq objVisitReq;

  SessionPage({Key key, @required this.objSite,
                        @required this.objGuest,
                        @required this.objContact,
                        @required this.objVisitReq}) : super(key: key);

  @override
  _SessionPageState createState() => new _SessionPageState();
}

class _SessionPageState extends State<SessionPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Scaffold stagger;
  Future myFutureVisit;
  CVisit objVisit;
  bool _isLoading = false;

  String getCurrentDate(String strFormat, String localization){
    var now = DateTime.now();
    return DateFormat(strFormat, localization).format(now);
  }

  Future <void> do_save_session(CVisit objVisit) async{
    //print('sessionPage :: save_session');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString("siteCode", objVisit.objSite.siteCode);
      preferences.setString("NoKTP", objVisit.objGuest.guestCardId);
      preferences.commit();
    });
  }

  Future<CVisit> _callServiceRegisVisit(CVisitSaveReq objVisitSaveReq) async{
    CVisit result;
    svcVisit visitService = new svcVisit();
    await visitService.setRegisterVisit(objVisitSaveReq)
        .then((CVisit objResult) {
          // if succeed register visit then mark it to shared pref as session
          do_save_session(objResult);
          setState(() {
            objVisit = objResult;
          });
      result = objResult;
    })
        .catchError((e){
    });
    return result;
  }

  Future<CVisit> _callServiceReloadVisit(CVisitSaveReq objVisitSaveReq) async{
    CVisit result;
    svcVisit visitService = new svcVisit();
    await visitService.getVisitById(objVisitSaveReq.visitid.toString())
        .then((CVisit objResult) {
          setState(() {
              objVisit = objResult;
          });
      result = objResult;
    }).catchError((e){ });
    return result;
  }

  Future<CCheckLog> do_checkInOut(BuildContext context, CSite objSite, CCheckLogReq objCheckLogReq) async{
    var labelCheckLogSuccess         = tr('SessionPage.do_checkInOut->labelCheckLogSuccess');
    var labelCheckLogFailed          = tr('SessionPage.do_checkInOut->labelCheckLogFailed');

    CCheckLog result;
    svcCheckLog checkLogService = new svcCheckLog();
    await checkLogService.setDoCheckLog(objCheckLogReq)
        .then((CCheckLog objResult) {
      if(objResult.checkLogId > 0){
        //Toast_Popup(context, Icon(Icons.done, color: Colors.white,), Colors.blue, objResult.checkTyped + " Success");
        Toast_Popup(context, Icon(Icons.done, color: Colors.white,), Colors.blue, objResult.checkTyped + ' $labelCheckLogSuccess');
      }
      else{
        //Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Visit Already Ended\nCheck Log Failed");
        Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, '$labelCheckLogFailed');
        // delay 3 sec to clear session
        Timer(Duration(seconds: 3), () {
          do_clear_session(context, objSite);
        });
      }
      result = objResult;
    })
        .catchError((e){
    });
    return result;
  }

  Future do_redirect_page(BuildContext context, CSite objSite) async{
    //print('sessionPage :: redirect to home');
    svcSite siteService = new svcSite();
    await siteService.getSiteInfoByCode(objSite.siteCode)
        .then((CSite objResultSite) {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) {
                    //print('do_redirect_page :');
                    //print('objSite.siteCode ->' + objResultSite.siteCode);
                    //print('objSite.siteCode ->' + objResultSite.formaccessmentid.toString());
                    return HomePage(objSite: objResultSite);
                }
                ));
    });
  }
  Future <void> do_clear_session(BuildContext context, CSite objSite) async{
    //print('sessionPage :: clear_session');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear().then((value){
      // move to home again
      //Future.delayed(Duration(seconds: 3), do_redirect_page);
      Timer(Duration(seconds: 3), () {
          do_redirect_page(context, objSite);
      });

    });
  }

  Future<CVisit> do_endVisit(BuildContext context, CSite objSite, CVisitEndReq objEndVisitReq) async{
    var labelVisitEnded             = tr('SessionPage.do_endVisit->labelVisitEnded');
    var labelVisitEndedConf         = tr('SessionPage.do_endVisit->labelVisitEndedConf');

    CVisit result;
    svcVisit visitService = new svcVisit();
    await visitService.setEndVisit(objEndVisitReq)
        .then((CVisit objResult) {
          // ended session success
          if(objResult.visitId > 0){
            // Toast_Popup(context, Icon(Icons.done, color: Colors.white,), Colors.blue, "Visit Ended Successfully");
            Toast_Popup(context, Icon(Icons.done, color: Colors.white,), Colors.blue, '$labelVisitEnded');
          }
          else{
            // session already inactive
            //Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Visit Already Ended");
            Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, '$labelVisitEndedConf');
          }
          // delay 3 sec
          Timer(Duration(seconds: 3), () {
              do_clear_session(context, objSite);
          });
          result = objResult;
        })
        .catchError((e){
    });
    return result;
  }

  void goToLogList(BuildContext context, String strVisitId)async {
    await Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
            ListLogPage(strVisitId: strVisitId)
            ));

  }

  showEndSessionDialog(BuildContext context, CSite objSite){
      var labelTitle             = tr('SessionPage.showEndSessionDialog->title');
      var labelDescription       = tr('SessionPage.showEndSessionDialog->description');

      showDialog(
          context: context,builder: (_) => AssetGiffyDialog(
              // title: Text('Konfirmasi Akhiri Kunjungan'),
              title: Text('$labelTitle'),
              //description: Text('Lanjutkan Akhiri kunjungan ?'),
              description: Text('$labelDescription'),
              image: Image.asset('assets/images/registersuccessdialog.png'),
              onlyOkButton: false,
              onOkButtonPressed: () {
                if (_isLoading == false) {
                  setState(() {
                    _isLoading = true;
                  });
                  do_endVisit(context,
                      objSite,
                      CVisitEndReq(
                          endVisitId: objVisit.visitId,
                          endVisitreason: '',
                          endVisitby: widget.objGuest.guestName
                      )
                  ).whenComplete(() {
                    setState(() {
                      _isLoading = false;
                    });
                    Navigator.pop(context);
                  }
                  );
                }
              },
              onCancelButtonPressed:  (){
                Navigator.of(context).pop();
              },
      ));
  }

  Widget Toast_Popup(BuildContext context, Icon pIcon, Color pColor, String strContent){
    AchievementView(
      context,
      title: "", //strTitle,
      icon: pIcon, // Icon(Icons.done, color: Colors.white,)
      color: pColor, //Colors.blue,
      alignment: Alignment.bottomCenter,
      subTitle: strContent,
      listener: (status) {
        print(status);
      },
    )..show();
  }

  Widget CardVisitWarning(BuildContext context, Color boxColor, IconData boxIcon, String strMessage) {
    return InkWell(
      child:  Card(
        elevation: 2,
        child: Container(
          padding: EdgeInsets.all(8),
          color: boxColor,
          height: 50,
          width: 500,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(flex: 3,
                child: Icon(boxIcon, size: 24,
                    color: Colors.yellow),
              ),
              Expanded(flex: 7,
                child: Text(strMessage,
                    style: TextStyle(
                        fontSize: 12,
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Raleway'))
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget Card_visitor(BuildContext context, CSite objSite, CVisit objVisit) {
    var label             = tr('SessionPage.Card_visitor->label');

    return Card(
      elevation: 4.0,
      color: Colors.black ,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(14),
      ),
      child: Container(
          //height: 200,
          height: MediaQuery.of(context).size.width >= 500 ? 200 : MediaQuery.of(context).size.width/2.0,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0, top: 20.0),
          child: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('Visitor Card',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            //fontSize: 25,
                            fontSize: MediaQuery.of(context).size.width >= 500 ? 25 : 20,
                            fontFamily: 'CourrierPrime',
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      Text('PT. Gudang Garam Tbk',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            //fontSize: 16,
                            fontSize: MediaQuery.of(context).size.width >= 500 ? 16 : 12,
                            fontFamily: 'CourrierPrime'),
                      ),
                      Text(objVisit.objSite.siteName,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            //fontSize: 16,
                            fontSize: MediaQuery.of(context).size.width >= 500 ? 16 : 12,
                            fontFamily: 'CourrierPrime'),
                      ),
                      SizedBox(height: 40.0),
                      Text(objVisit.objGuest.guestName, //objGuest.guestName,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            //fontSize: 16,
                            fontSize: MediaQuery.of(context).size.width >= 500 ? 16 : 12,
                            fontFamily: 'CourrierPrime',
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      Text(objVisit.objGuest.guestCompany,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            //fontSize: 14,
                            fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
                            fontFamily: 'CourrierPrime'),
                      ),
                      Text(objVisit.objGuest.guestPhone,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            //fontSize: 14,
                            fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
                            fontFamily: 'CourrierPrime'),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  flex: 3,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text('Scan QR for check-in/out', //objGuest.guestPhone,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.white,
                              //fontSize: 10,
                              fontSize: MediaQuery.of(context).size.width >= 500 ? 10 : 8,
                              fontFamily: 'CourrierPrime'),
                        ),
                        SizedBox(height: 2.0),
                        Image_QR(objVisit.visitSessionCode),
                        SizedBox(height: 2.0),
                        Column(
                            children: [
                              Text(DateFormat('dd-MMM-yyyy').format(DateTime.parse(objVisit.visitDate)), //objGuest.guestPhone,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.white,
                                    //fontSize: 10,
                                    fontSize: MediaQuery.of(context).size.width >= 500 ? 10 : 8,
                                    fontFamily: 'CourrierPrime'),
                              ),
                            ]
                        ),
                        SizedBox(height: 5.0),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(objVisit.visitSessionCode, //objGuest.guestPhone,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.white,
                                    //fontSize: 10,
                                    fontSize: MediaQuery.of(context).size.width >= 500 ? 10 : 8,
                                    fontFamily: 'CourrierPrime'),
                              )
                            ]
                        ),
                      ]
                  ),
                )
                //objVisirReq.visitId.toString()),
              ]
          )
      ), // end of container
    );
  }
  Widget Image_QR(String value) {
    return Container(
      // width: 100.0,
      // height: 100.0,
      width: MediaQuery.of(context).size.width >= 500 ? 100 : MediaQuery.of(context).size.width/5,
      height: MediaQuery.of(context).size.width >= 500 ? 100 : MediaQuery.of(context).size.width/5,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Center(
        child: QrImage(
          data: value,
          version: QrVersions.auto,
          size: MediaQuery.of(context).size.width >= 500 ? 98.0 : MediaQuery.of(context).size.width/5,
          gapless: false,
        )
        ),
      );
  }
  Widget BadgeSite(String strSite){
    return  Container(
      width: 120.0,
      height: 25.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        border: Border.all(width: 1.0, color: Colors.green),
      ),
      child: Center(
        child: Text(strSite,
          style: TextStyle(color: Colors.green),
        ),
      ),
    );
  }
  Widget DateBox(BuildContext context){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          getCurrentDate('EEEE, d MMM yyyy',context.locale.toString()),
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.width >= 500 ? 18: 16,
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontFamily: 'Raleway',
          ),
        ),
      ],
    );
  }

  Widget visit_contact(BuildContext context, CSite objSite, CVisit objVisit) {
    var labelKontak             = tr('SessionPage.visit_contact->labelKontak');
    var labelKontakCompany      = tr('SessionPage.visit_contact->labelKontakCompany');

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
            Row(
              children: [
                  Expanded(
                    flex: 7,
                    child: Column(
                      children: [
                        Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child:
                                    // Text('Nama Kontak',
                                    //   style: TextStyle(
                                    //       color: Colors.black,
                                    //       fontSize: 12.0,
                                    //       fontWeight: FontWeight.normal),
                                    // ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 1.0, bottom: 2.0),
                                      child: Text('$labelKontak',
                                        style: TextStyle(
                                            color: Colors.black,
                                            //fontSize: 12,
                                            fontSize: MediaQuery.of(context).size.width >= 500 ? 12 : 10,
                                            fontWeight: FontWeight.normal),
                                      ),
                                    ),
                                  ),
                                  //SizedBox(width: 5.0),
                                  Expanded(
                                    flex: 7,
                                    child:
                                      Padding(
                                        padding: const EdgeInsets.only(top: 1.0, bottom: 2.0),
                                        child: Text(widget.objContact.contactId == null ?
                                                  widget.objContact.contactAltName
                                                      :
                                                  widget.objContact.contactName,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        //fontSize: 12.0,
                                                        fontSize: MediaQuery.of(context).size.width >= 500 ? 12 : 10,
                                                        fontWeight: FontWeight.normal),
                                              ),
                                      ),
                                  )
                                ],
                              ),
                            ]
                        ),
                        Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child:
                                    // Text('Tenant Company ',
                                    //   style: TextStyle(
                                    //       color: Colors.black,
                                    //       fontSize: 12.0,
                                    //       fontWeight: FontWeight.normal),
                                    // ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 1.0, bottom: 2.0),
                                      child: Text('$labelKontakCompany',
                                        style: TextStyle(
                                            color: Colors.black,
                                            //fontSize: 12.0,
                                            fontSize: MediaQuery.of(context).size.width >= 500 ? 12 : 10,
                                            fontWeight: FontWeight.normal),
                                      ),
                                    ),
                                  ),
                                  //SizedBox(width: 5.0),
                                  Expanded(
                                    flex: 7,
                                    child:
                                        Padding(
                                          padding: const EdgeInsets.only(top: 1.0, bottom: 2.0),
                                          child: Text(widget.objContact.tenantCompany == null ? '' : widget.objContact.tenantCompany,
                                            //Text(widget.objContact.siteSpecific,
                                            style: TextStyle(
                                                color: Colors.black,
                                                //fontSize: 12.0,
                                                fontSize: MediaQuery.of(context).size.width >= 500 ? 12 : 10,
                                                fontWeight: FontWeight.normal),
                                          ),
                                        ),
                                  )
                                ],
                              ),
                            ]
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child:
                        // prevent checkin-out in different site
                        objSite.siteCode == objVisit.objSite.siteCode ?
                        // Di Hide Dulu untuk Phase 2 Biar nga Rancu
                        // Column(
                        //     crossAxisAlignment: CrossAxisAlignment.center,
                        //     children: [
                        //         Cmd_Check(context, objSite)
                        //     ]
                        // )
                        Column() : Column()
                  )
              ],
            )
        ]
    );
  }
  Widget Cmd_EndVisit(BuildContext context, CSite objSite) {
    var label             = tr('SessionPage.Cmd_EndVisit->label');
    return InkWell(
        onTap: () {
            showEndSessionDialog(context, objSite);
        },
        child: new Container(
          width: 150.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.orange,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Text('End Visit', style: TextStyle(fontSize: 12.0, color: Colors.black ))
                Text('$label', style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Check(BuildContext context, CSite objSite) {
    var label             = tr('SessionPage.Cmd_Check->label');
    return InkWell(
        onTap: () {
          do_checkInOut(
              context,
              objSite,
              CCheckLogReq(
                  visitSessionCode: objVisit.visitSessionCode,
                  terminalId: widget.objGuest.guestName
              ));
        },
        child: new Container(
          //width: 135.0,
          width: MediaQuery.of(context).size.width >= 500 ? 135.0 : 80.0,
          height: 40.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Text('Check-In/Out', style: TextStyle(fontSize: 12.0, color: Colors.black ))
                Text('$label',
                    style: TextStyle(
                        //fontSize: 12.0,
                        fontSize: MediaQuery.of(context).size.width >= 500 ? 12.0 : 10.0,
                        color: Colors.black
                    )
                )
              ],
            ),
          ),
        ));
  }
  Widget Cmd_ViewLog(BuildContext context, CVisit objVisit) {
    var label             = tr('SessionPage.Cmd_ViewLog->label');
    return InkWell(
        onTap: () {
          print('CMD VIEW LOG -> ' + objVisit.visitId.toString());
          goToLogList(context, objVisit.visitId.toString());
        },
        child: new Container(
          width: 135.0,
          height: 30.0,
          decoration: new BoxDecoration(
            color: Colors.blue,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Text('View Log', style: TextStyle(fontSize: 12.0, color: Colors.black ))
                Text('$label', style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }

  Widget session_body(BuildContext context,CSite objSite, CVisit objVisit){
    var labelContactInfo  = tr('SessionPage.session_body->labelContactInfo');
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
            //padding: const EdgeInsets.all(20.0),
            padding: MediaQuery.of(context).size.width >= 500 ? const EdgeInsets.all(20.0) : const EdgeInsets.all(5.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          flex: 6,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BadgeSite(objSite.siteName),
                                SizedBox(height: 10),
                                DateBox(context)
                              ]
                          )
                      ),
                      Expanded(
                          flex: 3,
                          child:
                          // prevent checkin-out in different site
                          objSite.siteCode == objVisit.objSite.siteCode ?
                          // Di Hide Dulu untuk Phase 2 Biar nga Rancu
                          // Column(
                          //   crossAxisAlignment: CrossAxisAlignment.end,
                          //   children: [
                          //     Cmd_ViewLog(context, objVisit),
                          //   ],
                          //)
                          Column() : Column()
                      ),
                    ],
                  ),
                  Divider(),
                  Card_visitor(context, objSite, objVisit),
                  objSite.siteCode == objVisit.objSite.siteCode ?
                      Column():
                      Column(
                        children: [
                          Divider(),
                          CardVisitWarning(context, Colors.indigoAccent, Icons.warning, "You has active session on different site"),
                        ],
                      ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 1.0, bottom: 1.0, right: 1.0),
                        // child: Text('Visit Contact' ,
                        //   style: TextStyle(
                        //       color: Colors.black,
                        //       fontSize: MediaQuery.of(context).size.width >= 500 ? 16.0 : 12.0,
                        //       fontWeight: FontWeight.normal),
                        // ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('$labelContactInfo',
                              //textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: MediaQuery.of(context).size.width >= 500 ? 16.0 : 12.0,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        )
                      ),
                      Padding(
                          padding: const EdgeInsets.only(top: 1.0, bottom: 1.0, right: 1.0),
                          // child: visit_contact(context, objSite, objVisit)
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                  visit_contact(context, objSite, objVisit)
                              ],
                          ),
                      ),
                    ],
                  ),
                  Divider(),
                  objSite.siteCode == objVisit.objSite.siteCode ?
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Cmd_EndVisit(context, objSite)
                        ],
                      ) : Column(),
                ]
            )
        ),
      ],
    );
  }
  Widget session_page(BuildContext context, CSite objSite, CVisit objVisit){

    return Center(
        child: Container(
          margin: const EdgeInsets.all(10.0),
          color: Colors.transparent,
          width: 500,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width >= 500 ? 24.0 : 12.0,
                right: MediaQuery.of(context).size.width >= 500 ? 24.0 : 12.0,
            ),
            children: <Widget>[
              // session_body(context, widget.objSite, objVisit)
              session_body(context, widget.objSite, objVisit)
            ],
          ),
        )
    ); // end container

  }

  @override
  void initState() {
    if(widget.objVisitReq.visitid == -1){
      // kalo visit id nya nga ada (baru)
      myFutureVisit = _callServiceRegisVisit(widget.objVisitReq);
    }
    else{
      // kalo visit id ada maka call reload visit
      myFutureVisit = _callServiceReloadVisit(widget.objVisitReq);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset('assets/images/logo.png',fit: BoxFit.cover, height: 40.0,),
              ],
            ),
            /*
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      showEndSessionDialog(context, widget.objSite);
                    },
                    child: Icon(
                      Icons.logout,
                      color: Colors.black,
                    ),
                  )
              ),
            ],
            */
          ),
          body: FutureBuilder(
              future: myFutureVisit,
              builder: (ctx, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                }
                else if(snapshot.connectionState == ConnectionState.done){
                    //print('snapshot: $snapshot');
                  if (snapshot.hasData) {
                    //print('C');
                    return session_page(context, widget.objSite, snapshot.data);
                  }
                  else{
                    //print('D');
                    return Text('No Data');
                  }
                }
                else {
                  //print('E');
                  return Center(child: CircularProgressIndicator() );
                }
              })
      ); //end scaffold
  }
}


