import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class NotFoundPage extends StatefulWidget {

  NotFoundPage({Key key}) : super(key: key);

  @override
  _NotFoundPageState createState() => new _NotFoundPageState();
}

class _NotFoundPageState extends State<NotFoundPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset('assets/images/logo.png',fit: BoxFit.cover, height: 40.0,),
            ],),
        ),
        body:
        Center(
            child: Container(
              margin: const EdgeInsets.all(10.0),
              color: Colors.transparent,
              width: 500.0,
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                  Image.asset('assets/images/page404.png',fit: BoxFit.cover),
                ],
              ),
            )
        )
    );
  }
}