import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestlogbook/model/CGuest.dart';
import 'package:myggguestlogbook/model/CSite.dart';
import 'package:myggguestlogbook/model/CContact.dart';
import 'package:myggguestlogbook/service/svcContact.dart';
import 'package:myggguestlogbook/screen/VisitPage.dart';
import 'package:easy_localization/easy_localization.dart';


class ListContactPage extends StatefulWidget {
  //final String siteparam;
  final CSite objSite;

  //final CGuest objGuest;
  // constructor

  /*
  ListContactPage({Key key,
    @required this.siteparam}) : super(key: key);
  */
  ListContactPage({Key key,
    @required this.objSite}) : super(key: key);

  /*
  ListContactPage({Key key,
    @required this.siteparam,
    @required this.objGuest}) : super(key: key);
  */

  @override
  _ListContactPageState createState() => new _ListContactPageState();
}

class _ListContactPageState extends State<ListContactPage> {
  TextEditingController controller = new TextEditingController();
  List<CContact> _searchResult = [];
  List<CContact> _contactList = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<void> getLoadInitService(strSiteCode) async{
    final svcContact  contactService = svcContact();
    // load service contact
    await contactService.getListContactBySite(strSiteCode).then((objResult) {
      if (objResult != null) {
        setState(() {
          _contactList = objResult;
        });
      }
    }).catchError((e) {
      //print("Error Load Contact List -> " + e.toString());
    });
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _contactList.forEach((CContact) {
      if (CContact.contactName.toUpperCase().contains(text.toUpperCase()))
        _searchResult.add(CContact);
    });
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getLoadInitService(widget.objSite.siteCode);
  }

  Widget SearchBar(BuildContext context){
    var hintLabel         = tr('ListContactPage.SearchBar->hintLabel');
    return Container(
      child: new Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Card(
          child: new ListTile(
            leading: new Icon(Icons.search),
            // title: new TextField(
            //   controller: controller,
            //   decoration: new InputDecoration(
            //       hintText: 'Cari Contact Person', border: InputBorder.none),
            //   onChanged: onSearchTextChanged,
            // ),
            title: new TextField(
              controller: controller,
              decoration: new InputDecoration(
                  hintText: '$hintLabel', border: InputBorder.none),
              onChanged: onSearchTextChanged,
            ),
            trailing: new IconButton(icon: new Icon(Icons.cancel),
              onPressed: () {
                controller.clear();
                onSearchTextChanged('');
              },
            ),
          ),
        ),
      ),
    );
  }
  Widget _buildRowListContact(BuildContext context, CContact objContact){
    return Padding(
      padding: EdgeInsets.only(bottom: 5.0),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        elevation: 3.0,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          child: Container(
            //height: deviceSize.height * 0.10,
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(
                  width: 10.0,
                  color: Colors.green  //strContent.bgStatus == 'ACT' ? Colors.green : Colors.red,
                ),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Text(objContact.tenantCompany == null ? '' : objContact.tenantCompany,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 12.0,
                        ),
                      ),
                    ),
                    SizedBox(width: 5.0),
                    Expanded(
                      flex: 6,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(objContact.contactName,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12.0,
                                  decoration: TextDecoration.none
                              ),
                            ),
                            //Text(objContact.contactExt,
                            //  style: TextStyle(
                            //    fontWeight: FontWeight.w500,
                            //    fontSize: 10.0,
                            //  ),
                            //),
                          ]
                      ),
                    ),
                    SizedBox(width: 5.0),
                    Expanded(
                      flex: 1,
                      child:  IconButton(
                        icon: Icon(Icons.arrow_forward_ios_sharp,
                            //size: 15.0,
                            color: Colors.blue
                        ),
                        highlightColor: Colors.pink,
                        onPressed: (){
                          Navigator.pop(context, objContact);
                          /*
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) =>
                                  VisitPage(objSite: widget.objSite,
                                      objContact: objContact)));
                          */
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }

  Widget ListViewContact(BuildContext context){
    return Expanded(
      child: _searchResult.length != 0 || controller.text.isNotEmpty ?
      ListView.builder(
        padding: EdgeInsets.only(top: 24.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _searchResult.length,
        itemBuilder: (context, i) {
          return _buildRowListContact(context, _searchResult[i]);
        },
      ) :
      ListView.builder(
        padding: EdgeInsets.only(top: 24.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _contactList.length,
        itemBuilder: (context, index) {
          return _buildRowListContact(context, _contactList[index]);
        },
      ),
    );
  }

  Widget buildList() {
    return  Column(
      children: <Widget>[
        SearchBar(context),
        Divider(),
        ListViewContact(context),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Contact List'),
        elevation: 0.0,
      ),
      body:
      Center(
        child: Container(
          margin: const EdgeInsets.all(10.0),
          color: Colors.transparent,
          width: 500.0,
          height: double.infinity,
          child: buildList(),
        ),
      ),
    );
    //);
  }
}


