import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:achievement_view/achievement_view.dart';
import 'package:myggguestlogbook/model/CGuest.dart';
import 'package:myggguestlogbook/model/CContact.dart';
import 'package:myggguestlogbook/model/CSite.dart';
import 'package:myggguestlogbook/model/CLookup.dart';
import 'package:myggguestlogbook/model/CVisitSaveReq.dart';
import 'package:myggguestlogbook/model/CQuestionaire.dart';
import 'package:myggguestlogbook/service/svcGuest.dart';
import 'package:myggguestlogbook/screen/SessionPage.dart';
import 'package:myggguestlogbook/screen/HomePage.dart';
import 'package:myggguestlogbook/screen/ListContactPage.dart';
import 'package:myggguestlogbook/tools/dialogBox.dart';
import 'package:myggguestlogbook/validation/valVisit.dart';
import 'package:myggguestlogbook/validation/valPSK.dart';
import 'package:easy_localization/easy_localization.dart';

class VisitPage extends StatefulWidget {
  final CSite objSite;
  final CGuest objGuest;
  final List<CQuestionaire> objQue;
  final List<CLookup> objDoc;

  Future fVisitInfo;
  VisitPage({Key key,
    @required this.objSite,
    @required this.objGuest,
    @required this.objDoc,
    @required this.objQue}) : super(key: key);

  @override
  _VisitPageState createState() => new _VisitPageState();
}

class _VisitPageState extends State<VisitPage> with valVisit{
  int _currentstep = 0;
  List<Step> _stepListW;

  bool isSwitched = false;
  final dialogBox message           = dialogBox();
  final valPSK validationPSKSub     = valPSK();

  final scaffoldStateKey  = GlobalKey<ScaffoldState>();
  final formVisitKey      = GlobalKey<FormState>();
  final formDeklarasiKey  = GlobalKey<FormState>();
  final formAssuranceKey  = GlobalKey<FormState>();
  final formAuthentKey    = GlobalKey<FormState>();

  CContact objSelectedContact;

  FocusNode NodeVisitContactName    = FocusNode();
  FocusNode NodeVisitPurpose        = FocusNode();
  FocusNode NodeContactEmail        = FocusNode();
  FocusNode NodeSwitch              = FocusNode();
  FocusNode NodeCmdSubmit           = FocusNode();
  FocusNode NodeNoDocAssurance      = FocusNode();
  FocusNode NodePSKeySub            = FocusNode();

  TextEditingController _ctrlVisitContactName     = TextEditingController();
  TextEditingController _ctrlVisitPurpose         = TextEditingController();
  TextEditingController _ctrlContactEmail         = TextEditingController();
  TextEditingController _ctrlNoDocAssurance       = TextEditingController();
  TextEditingController _ctrlPSKeySub             = TextEditingController();

  String strVisitContactName    = '';
  String strVisitPurpose        = '';
  String strContactEmail   = '';
  String strNoDocAssurance = '';
  String strQuestionaire;
  String strPSKeySub = '';

  bool checkNotListed = false;
  String docValueSelected = '';
  int docIndexSelected = 1;

  List<String> radioValues = [];
  List<CQuestionaire> filledQue = [];

  // /////////////////////////////////////////////////////////////////////////////////////////
  showProgressDialog(BuildContext context){
    var labelSubmitInprogress   = tr('VisitPage.showProgressDialog->labelErrNotComplete');

    AlertDialog alert=AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5),
              // child:Text("Please Wait ... Submit Inprogress")
              child:Text('$labelSubmitInprogress')
          ),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }
  Widget Toast_Popup(BuildContext context, Icon pIcon, Color pColor, String strContent){
    AchievementView(
      context,
      title: "Form Validation", //strTitle,
      icon: pIcon,
      color: pColor, //Colors.blue,
      alignment: Alignment.bottomCenter,
      subTitle: strContent,
      listener: (status) {
        print(status);
      },
    )..show();
  }

  // void _stepOnContinue() {
  //   var labelErrNotComplete             = tr('VisitPage.stepOnContinue->labelErrNotComplete');
  //   var labelErrNotChoosen               = tr('VisitPage.stepOnContinue->labelErrNotChoosen');
  //
  //   int que_blank = 0;
  //   int idx = 0;
  //   if(widget.objSite.formaccessmentid > 0){
  //     setState(() {
  //       // using que
  //       if (_currentstep < _stepListW.length - 1) {
  //         if (_currentstep == 0 && formVisitKey.currentState.validate()) {
  //           formVisitKey.currentState.save();
  //           _currentstep = _currentstep + 1;
  //         }
  //         else if (_currentstep == 1 && formDeklarasiKey.currentState.validate()) {
  //           formDeklarasiKey.currentState.save();
  //           idx = 0;
  //           widget.objQue.forEach((CQuestionaire que) {
  //             // filled questionaire
  //             filledQue.add(CQuestionaire(
  //                 quehid: que.quehid,
  //                 quedid: que.quedid,
  //                 quelineno: que.quelineno,
  //                 quetext: que.quetext,
  //                 quehname: que.quehname,
  //                 quehdesc: que.quehdesc,
  //                 queanswer: radioValues[idx].toString()
  //             ));
  //
  //             if(radioValues[idx] == ''){
  //               que_blank = que_blank + 1;
  //             }
  //             idx = idx + 1;
  //           });
  //
  //           strQuestionaire = jsonEncode(filledQue);
  //           print(strQuestionaire);
  //           // if its all filled then allow to move next step
  //           if(que_blank == 0){
  //             _currentstep = _currentstep + 1;
  //           }
  //           else{
  //             //Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Lengkap');
  //             Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, '$labelErrNotComplete');
  //           }
  //         }
  //         else if (_currentstep == 2 && formAssuranceKey.currentState.validate()) {
  //           formAssuranceKey.currentState.save();
  //           //print('docValueSelected  -> ' + docValueSelected);
  //           //print('strNoDocAssurance -> ' + strNoDocAssurance.toString());
  //
  //           if(docValueSelected == ''){
  //             // Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Dipilih');
  //             Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, '$labelErrNotChoosen');
  //           }
  //           else{
  //             _currentstep = _currentstep + 1;
  //           }
  //         }
  //       }
  //       else {
  //         if (_currentstep == 3) {
  //           //print('submit');
  //           goToSessionPage(context);
  //         }
  //       }
  //       print(_currentstep);
  //     });
  //   }
  //   else{
  //     // this whithout que
  //     setState(() {
  //       if (_currentstep < _stepListW.length - 1) {
  //         if (_currentstep == 0 && formVisitKey.currentState.validate()) {
  //           strQuestionaire = null;
  //           formVisitKey.currentState.save();
  //           _currentstep = _currentstep + 1;
  //         }
  //         else if (_currentstep == 1 && formAssuranceKey.currentState.validate()) {
  //           formAssuranceKey.currentState.save();
  //           //print('docValueSelected  -> ' + docValueSelected);
  //           //print('strNoDocAssurance -> ' + strNoDocAssurance.toString());
  //
  //           if(docValueSelected == ''){
  //             // Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Dipilih');
  //             Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, '$labelErrNotChoosen');
  //           }
  //           else{
  //             _currentstep = _currentstep + 1;
  //           }
  //         }
  //       }
  //       else {
  //         if (_currentstep == 2) {
  //           //print('submit');
  //           goToSessionPage(context);
  //         }
  //       }
  //       print(_currentstep);
  //     });
  //   }
  // }

  void _stepOnContinue() {
    var labelErrNotComplete              = tr('VisitPage.stepOnContinue->labelErrNotComplete');
    var labelErrNotChoosen               = tr('VisitPage.stepOnContinue->labelErrNotChoosen');

    int que_blank = 0;
    int idx = 0;
    if(widget.objSite.formaccessmentid > 0){
      setState(() {
        // using que
        if (_currentstep < _stepListW.length - 1) {
          if (_currentstep == 0 && formVisitKey.currentState.validate()) {
            formVisitKey.currentState.save();
            _currentstep = _currentstep + 1;
          }
          else if (_currentstep == 1 && formDeklarasiKey.currentState.validate()) {
            formDeklarasiKey.currentState.save();
            idx = 0;
            widget.objQue.forEach((CQuestionaire que) {
              // filled questionaire
              filledQue.add(CQuestionaire(
                  quehid: que.quehid,
                  quedid: que.quedid,
                  quelineno: que.quelineno,
                  quetext: que.quetext,
                  quehname: que.quehname,
                  quehdesc: que.quehdesc,
                  queanswer: radioValues[idx].toString()
              ));

              if(radioValues[idx] == ''){
                que_blank = que_blank + 1;
              }
              idx = idx + 1;
            });

            strQuestionaire = jsonEncode(filledQue);
            print(strQuestionaire);
            // if its all filled then allow to move next step
            if(que_blank == 0){
              _currentstep = _currentstep + 1;
            }
            else{
              //Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Lengkap');
              Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, '$labelErrNotComplete');
            }
          }
        }
        else {
          if (_currentstep == 2 && formAssuranceKey.currentState.validate()) {
            formAssuranceKey.currentState.save();
            if(docValueSelected == ''){
              // Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Dipilih');
              Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, '$labelErrNotChoosen');
            }
            else{
              goToSessionPage(context);
            }
          }
        }
        print(_currentstep);
      });
    }
    else{
      // this whithout que
      setState(() {
        if (_currentstep < _stepListW.length - 1) {
          if (_currentstep == 0 && formVisitKey.currentState.validate()) {
            strQuestionaire = null;
            formVisitKey.currentState.save();
            _currentstep = _currentstep + 1;
          }
        }
        else {
          if (_currentstep == 1 && formAssuranceKey.currentState.validate()) {
            formAssuranceKey.currentState.save();
            if(docValueSelected == ''){
              // Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Dipilih');
              Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, '$labelErrNotChoosen');
            }
            else{
              goToSessionPage(context);
            }
          }
        }
        print(_currentstep);
      });
    }
  }

  void _stepOnCancel() {
    setState(() {
      if (_currentstep > 0) {
        _currentstep = _currentstep - 1;
      }
      else {
        _currentstep = 0;
        goToHomePage(context);
      }
      //print(_currentstep);
    });
  }
  void _stepOnSelect(int step) {
    setState(() {
      if (_currentstep == 0 && formVisitKey.currentState.validate()) {
        _currentstep = step;
      }
      //print(_currentstep);
    });
  }

  void goToContactList(BuildContext context)async {
    CContact selectedContact = await Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
            ListContactPage(objSite: widget.objSite
            )));

    setState(() {
      _ctrlVisitContactName.text = selectedContact.contactName;
      objSelectedContact = CContact(
          contactId: selectedContact.contactId,
          siteCode: selectedContact.siteCode,
          siteSpecific: selectedContact.siteSpecific,
          contactName: _ctrlVisitContactName.text,
          contactEmail: selectedContact.contactEmail,
          contactExt: selectedContact.contactExt
      );
    });
  }
  void goToHomePage(BuildContext context) async {
    await Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) {
          return HomePage(objSite: widget.objSite);
        }));
  }
  void goToSessionPage(BuildContext context) async {
      await Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_) {
            return SessionPage(
                objSite: widget.objSite,
                objGuest: widget.objGuest,
                objContact: objSelectedContact,
                objVisitReq: CVisitSaveReq(
                    siteid: widget.objSite.siteId,
                    guestid: widget.objGuest.guestId,
                    contactid: objSelectedContact.contactId,
                    visitid: -1,
                    // to ensure this is new request not reload
                    visitpurpose: strVisitPurpose,
                    contactaltname: (checkNotListed == true)
                        ? strVisitContactName
                        : null,
                    assuranceCardType: docValueSelected,
                    assuranceCardNo: strNoDocAssurance,
                    formaccessment: strQuestionaire
                )
            );
          }));
  }

  void clearContactSelected(BuildContext context){
    setState(() {
      // clear text input
      _ctrlVisitContactName.clear();
      // clear object selected
      objSelectedContact = CContact(
          contactId: null,
          siteCode: null,
          siteSpecific: null,
          contactName: null,
          contactEmail: null,
          contactExt: null,
          contactAltName: null
      );
    });
  }

  Widget Lbl_VisitInfo(BuildContext context, CGuest objGuest) {
    //var label = 'VisitPage.Lbl_VisitInfo->label'.tr(args: [objGuest.guestName]);
    var label             = tr('VisitPage.Lbl_VisitInfo->label', args: [objGuest.guestName]);
    //var label             = tr('VisitPage.Lbl_VisitInfo->label', args: ['name', objGuest.guestName]);
    //var label             = tr('VisitPage.Lbl_VisitInfo->label', args: [objGuest.guestName]);
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          //fontSize: 26.0,
          fontSize: MediaQuery.of(context).size.width >= 500 ? 22.0 : 20.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          // new TextSpan(text: 'Hi, ' + objGuest.guestName + '\nSilahkan Informasikan \nkunjungan anda')
          new TextSpan(text: '$label')
        ],
      ),
    );
  }
  Widget Lbl_FormDeklarasi(BuildContext context, String title) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          //fontSize: 16.0,
          fontSize: MediaQuery.of(context).size.width >= 500 ? 22.0 : 20.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          //new TextSpan(text: 'FORM SELF ASSESMENT PENGUNJUNG/TRACING')
          new TextSpan(text: '$title')
        ],
      ),
    );
  }
  Widget Lbl_DocumentAssurance(BuildContext context) {
    var label             = tr('VisitPage.Lbl_DocumentAssurance->label');
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          //fontSize: 26.0,
          fontSize: MediaQuery.of(context).size.width >= 500 ? 22.0 : 20.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          // new TextSpan(text: 'Silahkan pilih salah satu jenis identitas berikut untuk di tukarkan dengan access card')
           new TextSpan(text: '$label')
        ],
      ),
    );
  }
  Widget Lbl_SubmitAsk(BuildContext context) {
    var label             = tr('VisitPage.Lbl_SubmitAsk->label');
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          //fontSize: MediaQuery.of(context).size.width >= 500 ? 16.0 : 15.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          // new TextSpan(text: 'Lanjutkan dengan mensubmit data \n dan mendapatkan Visit Session Kunjungan Anda')
          new TextSpan(text: '$label')
        ],
      ),
    );
  }
  Widget SiteBadge(BuildContext context, CSite objSite) {
    return Padding(
        padding: EdgeInsets.only(top: 5.0, left: 100.0, right: 100.0, bottom: 5.0),
        child: InkWell(
            child: new Container(
              width: 130.0,
              height: 20.0,
              decoration: new BoxDecoration(
                color: Colors.blue,
                border: new Border.all(color: Colors.white, width: 2.0),
                borderRadius: new BorderRadius.circular(10.0),
              ),
              child: new Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(objSite.siteName, style: TextStyle(fontSize: 10.0, color: Colors.white ))
                  ],
                ),
              ),
            ))
    );
  }
  Widget Txt_Visit_Contact(BuildContext context) {
    var label             = tr('VisitPage.Txt_Visit_Contact->label');
    return Column(
      children: [
        TextFormField(
          controller: _ctrlVisitContactName,
          focusNode: NodeVisitContactName,
          keyboardType: TextInputType.text,
          validator: validateContactName,
          onSaved: (String value) {
            strVisitContactName = value;
          },
          readOnly: (checkNotListed == true) ? false : true,
          autofocus: false,
          decoration: InputDecoration(
            icon: const Icon(Icons.person),
            prefixIcon: checkNotListed == true ? null :
            IconButton(
              onPressed: () {
                // navigate to contact list
                goToContactList(context);
              },
              icon: Icon(Icons.list),
            ),
            suffixIcon: IconButton(
              onPressed: () {
                clearContactSelected(context);
              },
              icon: Icon(Icons.clear),
            ),
            // labelText: 'Nama orang yang di kunjungi',
            labelText: '$label',
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(NodeVisitPurpose);
          },
        ),
        Chk_Contact_Not_In_The_List(context),
      ],
    );
  }
  Widget Txt_Visit_Purpose(BuildContext context) {
    var label             = tr('VisitPage.Txt_Visit_Purpose->label');
    return TextFormField(
      controller: _ctrlVisitPurpose,
      focusNode: NodeVisitPurpose,
      validator: validateVisitPurpose,
      onSaved: (String value) {
        strVisitPurpose = value;
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.paid),
        // labelText: 'Tujuan kunjungan',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdSubmit);
      },
    );
  }
  Widget Txt_No_DocumentAssurance(BuildContext context) {
    var label             = tr('VisitPage.Txt_No_DocumentAssurance->label');
    return TextFormField(
      controller: _ctrlNoDocAssurance,
      focusNode: NodeNoDocAssurance,
      validator: validateNoDocAssurance,
      style: TextStyle(
          fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 13.0,
      ),
      onSaved: (String value) {
        strNoDocAssurance = value;
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.card_membership),
        // labelText: 'Nomor Identitas',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        //FocusScope.of(context).requestFocus(NodeCmdSubmit);
      },
    );
  }
  Widget Chk_Contact_Not_In_The_List(BuildContext context){
    var label             = tr('VisitPage.Chk_Contact_Not_In_The_List->label');
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.only(top: 5.0, left: 45.0, bottom: 5.0),
            child: Row(
              children: [
                Checkbox(
                  value: checkNotListed, //false, //this.value,
                  onChanged: (bool value) {
                    setState(() {
                      clearContactSelected(context);
                      checkNotListed = value;
                    });
                  },
                ),
                // Text('Contact tidak ada di list',
                //   style: TextStyle(
                //     fontWeight: FontWeight.w500,
                //     fontSize: 12.0,
                //   ),
                // ),
                Text('$label',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12.0,
                  ),
                ),
              ],
            )
        )
      ],
    );
  }
  Widget Cmd_Next(BuildContext context, VoidCallback onStepContinue) {
    var label             = tr('VisitPage.Cmd_Next->label');
    return InkWell(
        onTap: onStepContinue,
        child: new Container(
          width: 90.0,
          height: 30.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Text('Lanjut', style: TextStyle(fontSize: 12.0, color: Colors.black ))
                Text('$label', style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Back(BuildContext context, VoidCallback onStepCancel) {
    var label             = tr('VisitPage.Cmd_Back->label');
    return InkWell(
        onTap: onStepCancel,
        child: new Container(
          width: 90.0,
          height: 30.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Text('Kembali', style: TextStyle(fontSize: 12.0, color: Colors.black ))
                Text('$label', style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Txt_PSKey(BuildContext context) {
    var label             = tr('VisitPage.Txt_PSKey->label');
    return TextFormField(
      controller: _ctrlPSKeySub,
      focusNode: NodePSKeySub,
      keyboardType: TextInputType.text,
      validator: (value) {
        return validationPSKSub.validatePSK(value, widget.objSite);
      },
      style: TextStyle(
        fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 13.0,
      ),
      onSaved: (String value) {
        strPSKeySub = value;
      },
      autofocus: false,
      decoration: InputDecoration(
        //icon: const Icon(Icons.vpn_key),
        // labelText: 'Authentication Key',
        labelText: '$label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        //FocusScope.of(context).requestFocus(NodeCmdSubmit);
      },
    );
  }


  Widget _buildRadioGroup(BuildContext context, CGuest objGuest, List<CLookup> _docAssurance){
    //print('_buildRadioGroup');
    return Column(
      children:
      _docAssurance.map((doc) => RadioListTile(
        // MediaQuery.of(context).size.width >= 500 ?
        title: Text(doc.lookupValue,
            style: new TextStyle(
              fontSize: MediaQuery.of(context).size.width >= 500 ? 16.0 : 14.0,
              color: Colors.black,
            )
        ),
        subtitle: Text(doc.lookupDescription,
            style: new TextStyle(
              fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 12.0,
              color: Colors.black,
            )
        ),
        value: doc.lookupValue,
        groupValue:  docValueSelected,
        onChanged: (value) {
          setState(() {
            docValueSelected = value;
            // clear inputan jika pindah
            _ctrlNoDocAssurance.clear();

            // if doc selected as KTP then filled default with guest card id
            if(value.toString().toUpperCase() == 'KTP' || value.toString().toUpperCase() == 'PASPORT'){
              _ctrlNoDocAssurance  = TextEditingController(text: objGuest.guestCardId);
            }
          });
        },
      )).toList(),
    );
  }
  Widget _buildQuestionaire(BuildContext context, List<CQuestionaire> _queList){
    //print('_buildQuestionaire -> ' + _queList.length.toString());
    var labelNo             = tr('VisitPage.buildQuestionaire->labelNo');
    var labelPertanyaan     = tr('VisitPage.buildQuestionaire->labelPertanyaan');
    var labelYa             = tr('VisitPage.buildQuestionaire->labelYa');
    var labelTidak          = tr('VisitPage.buildQuestionaire->labelTidak');

    return DataTable(
        columnSpacing: 10,
        dataRowHeight: 100,
        columns: [
          DataColumn(
            // label: Text("No",
            //     style: new TextStyle(
            //       fontSize: 14.0,
            //       color: Colors.black,
            //     )
            // ),
            label: Text('$labelNo',
                style: new TextStyle(
                  fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 11.0,
                  color: Colors.black,
                )
            ),
            numeric: true,
          ),
          DataColumn(
            // label: Text("Pertanyaan",
            //   style: new TextStyle(
            //     fontSize: 14.0,
            //     color: Colors.black,
            //   ),
            //   textAlign: TextAlign.center,
            // ),
            label: Text('$labelPertanyaan',
              style: new TextStyle(
                fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 11.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          DataColumn(
            // label: Text("Ya",
            //   style: new TextStyle(
            //     fontSize: 14.0,
            //     color: Colors.black,
            //   ),
            //   textAlign: TextAlign.center,
            // ),
            label: Text('$labelYa',
              style: new TextStyle(
                fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 11.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          DataColumn(
            // label: Text("Tidak",
            //   style: new TextStyle(
            //     fontSize: 14.0,
            //     color: Colors.black,
            //   ),
            //   textAlign: TextAlign.center,
            // ),
            label: Text('$labelTidak',
              style: new TextStyle(
                fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 11.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
        rows: _queList.map((que) =>
            DataRow(
                cells: [
                  DataCell(
                      Container(
                        width: MediaQuery.of(context).size.width >= 500 ? 15 : 12,
                        child: Text(que.quelineno.toString(),
                          style: new TextStyle(
                          fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 12.0,
                        ),
                        ),
                      )
                  ),
                  DataCell(
                      Container(
                          width: MediaQuery.of(context).size.width >= 500 ? 200 : 150,
                          child:  Text(que.quetext.toString(),
                            style: new TextStyle(
                              fontSize: MediaQuery.of(context).size.width >= 500 ? 14.0 : 12.0,
                            ),
                          )
                      )
                  ),
                  DataCell(
                    Container(
                      width: 15,
                      child:  Radio(
                        value: 'Y',
                        groupValue: radioValues[que.quelineno-1],
                        onChanged: (value) {
                          setState(() {
                            //print('Y -> ' + value);
                            radioValues[que.quelineno-1] = value;
                          });
                        },
                      ),
                    ),
                  ),
                  DataCell(
                    Container(
                      width: 15,
                      child: Radio(
                        value: 'N',
                        groupValue: radioValues[que.quelineno-1],
                        onChanged: (value) {
                          setState(() {
                            //print('N -> ' + value);
                            radioValues[que.quelineno-1] = value;
                          });
                        },
                      ),
                    ),
                  )
                ]
            )).toList()
    );
  }


  Widget form_Visit(BuildContext context) {
    //print('form_Visit');
    return Form(
        key: formVisitKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 5.0),
            Txt_Visit_Contact(context),
            SizedBox(height: 5.0),
            Txt_Visit_Purpose(context),
            SizedBox(height: 5.0),
          ],
        )
    );
  }
  Widget form_deklarasi(BuildContext context, CSite objSite) {
    //print('form_deklarasi');
    var queTitle = widget.objQue.first.quehdesc;

    return Form(
        key: formDeklarasiKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20.0),
            Lbl_FormDeklarasi(context, '$queTitle'),
            //SiteBadge(context, widget.objSite),
            SiteBadge(context, objSite),
            SizedBox(height: 20.0),
            _buildQuestionaire(context, widget.objQue),
          ],
        )
    );
  }
  Widget form_doc_assurance(BuildContext context, CGuest objGuest) {
    //print('form_doc_assurance');
    return Form(
        key: formAssuranceKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildRadioGroup(context, objGuest, widget.objDoc),
            SizedBox(height: 5.0),
            Txt_No_DocumentAssurance(context),
            SizedBox(height: 5.0),
            Divider(),
            SizedBox(height: 5.0),
            Padding(
                child: Txt_PSKey(context),
                padding: EdgeInsets.only(left: 80, right:80)
            ),
            SizedBox(height: 10.0),
          ],
        )
    );
  }

  Widget SectionVisitInfo(BuildContext context, CSite objSite, CGuest objGuest){
    return Column(
      children: [
        SizedBox(height: 20.0),
        //Lbl_VisitInfo(context, widget.objGuest),
        Lbl_VisitInfo(context, objGuest),
        //SiteBadge(context, widget.objSite),
        SiteBadge(context, objSite),
        SizedBox(height: 20.0),
        form_Visit(context),
        //Legal_Section(context)
      ],
    );
  }
  Widget SectionDeklarasi(BuildContext context, CSite objSite){
    return Column(
      children: [
        // SizedBox(height: 20.0),
        // Lbl_FormDeklarasi(context),
        // //SiteBadge(context, widget.objSite),
        // SiteBadge(context, objSite),
        // SizedBox(height: 20.0),
        form_deklarasi(context, objSite),
      ],
    );
  }
  Widget SectionDocumentAssurance(BuildContext context, CSite objSite, CGuest objGuest){
    return Column(
      children: [
        SizedBox(height: 20.0),
        Lbl_DocumentAssurance(context),
        //SiteBadge(context, widget.objSite),
        SiteBadge(context, objSite),
        SizedBox(height: 20.0),
        form_doc_assurance(context, objGuest),
        //Legal_Section(context)
      ],
    );
  }

  Widget SectionSubmit(BuildContext context, CSite objSite, CGuest objGuest){
    var label  = tr('VisitPage.SectionSubmit->label');
    return Column(
      children: [
        SizedBox(height: 20.0),
        // Text('Submit'),
        Text('$label'),
        //form_authentikasi(context, objSite),
        //SizedBox(height: 20.0),
      ]
    );
  }
////////////////////////////////////////////////////////////////////////////////////////////

  // List<Step> _createStepsW(BuildContext context, CSite objSite, CGuest objGuest) {
  //   var labelTabInfo             = tr('VisitPage.createStepsW->labelTabInfo');
  //   var labelTabDeklarasi        = tr('VisitPage.createStepsW->labelTabDeklarasi');
  //   var labelTabDokumen          = tr('VisitPage.createStepsW->labelTabDokumen');
  //   var labelTabSubmit           = tr('VisitPage.createStepsW->labelTabSubmit');
  //
  //   List<Step> _steps;
  //
  //   if(objSite != null){
  //     if(objSite.formaccessmentid > 0){
  //       _steps = [
  //         Step(
  //           state: _currentstep == 0 ? StepState.editing : _currentstep > 0 ? StepState.complete : StepState.indexed,
  //           // title: new Text('Info'),
  //           title: new Text('$labelTabInfo',
  //             style: TextStyle(
  //                 color: Colors.black,
  //                 fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
  //                 fontFamily: 'CourrierPrime',
  //             ),
  //           ),
  //           content: SectionVisitInfo(context, objSite, objGuest),
  //           isActive: true,
  //         ),
  //         Step(
  //           state: _currentstep == 1 ? StepState.editing : _currentstep > 1 ? StepState.complete : StepState.indexed,
  //           // title: new Text('Deklarasi'),
  //           title: new Text('$labelTabDeklarasi',
  //             style: TextStyle(
  //               color: Colors.black,
  //               fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
  //               fontFamily: 'CourrierPrime',
  //             ),
  //           ),
  //           content: SectionDeklarasi(context, objSite),
  //           isActive: true,
  //         ),
  //         Step(
  //           state: _currentstep == 2 ? StepState.editing : _currentstep > 2 ? StepState.complete : StepState.indexed,
  //           // title: new Text('Dokumen'),
  //           title: new Text('$labelTabDokumen',
  //             style: TextStyle(
  //               color: Colors.black,
  //               fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
  //               fontFamily: 'CourrierPrime',
  //             ),
  //           ),
  //           content: SectionDocumentAssurance(context, objSite, objGuest),
  //           isActive: true,
  //         ),
  //         Step(
  //           state: StepState.complete,
  //           // title: new Text('Submit'),
  //           title: new Text('$labelTabSubmit',
  //             style: TextStyle(
  //               color: Colors.black,
  //               fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
  //               fontFamily: 'CourrierPrime',
  //             ),
  //           ),
  //           content: SectionSubmit(context, objSite, objGuest),
  //           //Text('Submit'), //Lbl_SubmitAsk(context),
  //           isActive: true,
  //         ),
  //       ];
  //     }
  //     else{
  //       _steps = [
  //         Step(
  //           state: _currentstep == 0 ? StepState.editing : _currentstep > 0 ? StepState.complete : StepState.indexed,
  //           // title: new Text('Info'),
  //           title: new Text('$labelTabInfo',
  //             style: TextStyle(
  //               color: Colors.black,
  //               fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
  //               fontFamily: 'CourrierPrime',
  //             ),
  //           ),
  //           content: SectionVisitInfo(context, objSite, objGuest),
  //           isActive: true,
  //         ),
  //         Step(
  //           state: _currentstep == 1 ? StepState.editing : _currentstep > 1 ? StepState.complete : StepState.indexed,
  //           // title: new Text('Dokumen'),
  //           title: new Text('$labelTabDokumen',
  //             style: TextStyle(
  //               color: Colors.black,
  //               fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
  //               fontFamily: 'CourrierPrime',
  //             ),
  //           ),
  //           content: SectionDocumentAssurance(context, objSite, objGuest),
  //           isActive: true,
  //         ),
  //         Step(
  //           state: StepState.complete,
  //           // title: new Text('Submit'),
  //           title: new Text('$labelTabSubmit',
  //             style: TextStyle(
  //               color: Colors.black,
  //               fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
  //               fontFamily: 'CourrierPrime',
  //             ),
  //           ),
  //           content: SectionSubmit(context, objSite, objGuest),
  //           //Text('Submit'), //Lbl_SubmitAsk(context),
  //           isActive: true,
  //         ),
  //       ];
  //     }
  //   }
  //   return _steps;
  // }

  List<Step> _createStepsW(BuildContext context, CSite objSite, CGuest objGuest) {
    var labelTabInfo             = tr('VisitPage.createStepsW->labelTabInfo');
    var labelTabDeklarasi        = tr('VisitPage.createStepsW->labelTabDeklarasi');
    var labelTabDokumen          = tr('VisitPage.createStepsW->labelTabDokumen');
    var labelTabSubmit           = tr('VisitPage.createStepsW->labelTabSubmit');

    List<Step> _steps;

    if(objSite != null){
      if(objSite.formaccessmentid > 0){
        _steps = [
          Step(
            state: _currentstep == 0 ? StepState.editing : _currentstep > 0 ? StepState.complete : StepState.indexed,
            // title: new Text('Info'),
            title: new Text('$labelTabInfo',
              style: TextStyle(
                color: Colors.black,
                fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
                fontFamily: 'CourrierPrime',
              ),
            ),
            content: SectionVisitInfo(context, objSite, objGuest),
            isActive: true,
          ),
          Step(
            state: _currentstep == 1 ? StepState.editing : _currentstep > 1 ? StepState.complete : StepState.indexed,
            // title: new Text('Deklarasi'),
            title: new Text('$labelTabDeklarasi',
              style: TextStyle(
                color: Colors.black,
                fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
                fontFamily: 'CourrierPrime',
              ),
            ),
            content: SectionDeklarasi(context, objSite),
            isActive: true,
          ),
          Step(
            state: _currentstep == 2 ? StepState.editing : _currentstep > 2 ? StepState.complete : StepState.indexed,
            //state: StepState.complete,
            // title: new Text('Dokumen'),
            title: new Text('$labelTabDokumen',
              style: TextStyle(
                color: Colors.black,
                fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
                fontFamily: 'CourrierPrime',
              ),
            ),
            content: SectionDocumentAssurance(context, objSite, objGuest),
            isActive: true,
          ),

          // shortened step complete
          // Step(
          //   state: StepState.complete,
          //   // title: new Text('Submit'),
          //   title: new Text('$labelTabSubmit',
          //     style: TextStyle(
          //       color: Colors.black,
          //       fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
          //       fontFamily: 'CourrierPrime',
          //     ),
          //   ),
          //   content: SectionSubmit(context, objSite, objGuest),
          //   //Text('Submit'), //Lbl_SubmitAsk(context),
          //   isActive: true,
          // ),

        ];
      }
      else{
        _steps = [
          Step(
            state: _currentstep == 0 ? StepState.editing : _currentstep > 0 ? StepState.complete : StepState.indexed,
            // title: new Text('Info'),
            title: new Text('$labelTabInfo',
              style: TextStyle(
                color: Colors.black,
                fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
                fontFamily: 'CourrierPrime',
              ),
            ),
            content: SectionVisitInfo(context, objSite, objGuest),
            isActive: true,
          ),
          Step(
            state: _currentstep == 1 ? StepState.editing : _currentstep > 1 ? StepState.complete : StepState.indexed,
            //state: StepState.complete,
            // title: new Text('Dokumen'),
            title: new Text('$labelTabDokumen',
              style: TextStyle(
                color: Colors.black,
                fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
                fontFamily: 'CourrierPrime',
              ),
            ),
            content: SectionDocumentAssurance(context, objSite, objGuest),
            isActive: true,
          ),

          // Step(
          //   state: StepState.complete,
          //   // title: new Text('Submit'),
          //   title: new Text('$labelTabSubmit',
          //     style: TextStyle(
          //       color: Colors.black,
          //       fontSize: MediaQuery.of(context).size.width >= 500 ? 14 : 10,
          //       fontFamily: 'CourrierPrime',
          //     ),
          //   ),
          //   content: SectionSubmit(context, objSite, objGuest),
          //   //Text('Submit'), //Lbl_SubmitAsk(context),
          //   isActive: true,
          // ),
        ];
      }
    }
    return _steps;
  }

  Widget _buildContent(BuildContext context){
    //print('_buildContent');
    return Center(
        child: Container(
            margin: const EdgeInsets.all(5.0),
            color: Colors.transparent,
            width: 500.0,
            child:  Stepper(
                        controlsBuilder: (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                          return Row(
                            children: <Widget>[
                              Cmd_Back(context, onStepCancel),
                              Cmd_Next(context, onStepContinue),
                            ],
                          );
                        },
                        steps:_stepListW,
                        type: StepperType.horizontal,
                        currentStep: _currentstep,
                        onStepContinue: _stepOnContinue,
                        onStepCancel: _stepOnCancel,
                        onStepTapped: _stepOnSelect,
                    )
          )
    );
  }

  @override
  void initState() {
    super.initState();
    //print(widget.objSite.formaccessmentid);
    // if site assigned by que form then do load init
    if(widget.objSite.formaccessmentid > 0){
      setState(() {
        widget.objQue.forEach((CQuestionaire que) {
          radioValues.add('');
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // init step list

    //_stepListW = _createStepsW(
    _stepListW = _createStepsW(
        context,
        widget.objSite,
        widget.objGuest);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => svcGuest(),
        )
      ],
      child: MaterialApp(
          home: new Scaffold(
              appBar: new AppBar(
                backgroundColor: Colors.white,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Image.asset('assets/images/logo.png',fit: BoxFit.cover, height: 40.0,),
                  ],
                ),
              ),
              body: _buildContent(context)
          ) // end scaffold
      ),
    );

  }
}