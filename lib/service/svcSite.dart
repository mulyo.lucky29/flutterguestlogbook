import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestlogbook/Global/Settings.dart';
import 'package:myggguestlogbook/model/CSite.dart';

class svcSite extends ChangeNotifier {

  Future<CSite> getSiteInfoByCode(String p_siteCode) async {
    final String url = baseUrl + '/site/getSiteByCode/' + p_siteCode;
    CSite result;
    //print(url);
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if (response.statusCode == 200) {
        final responseJson = json.decode(response.body);
        //print(response.body.toString());
        result = CSite.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch (err) {
      result = null;
      throw err;
    }
    return result;
  }
}