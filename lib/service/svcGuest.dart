import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestlogbook/Global/Settings.dart';
import 'package:myggguestlogbook/model/CGuest.dart';

class svcGuest extends ChangeNotifier {

  Future<CGuest> getGuestByKTP(String p_strNoKTP) async{
    final String url = baseUrl + '/guest/getGuestCardId/' + p_strNoKTP;
    CGuest result;
    //print (url);
    try{
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if(response.statusCode == 200){
        final responseJson = json.decode(response.body);
        //print(response.body.toString());
        result = CGuest.fromJson(responseJson);
      }
      else{
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CGuest> setRegisterGuest(CGuest param) async {
    CGuest result;
    final url = baseUrl + '/guest/saveGuestInfo';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    //print("setRegisterGuest -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      //print(response.body.toString());

      if (response.statusCode == 200) {
        result = CGuest.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }

    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

}