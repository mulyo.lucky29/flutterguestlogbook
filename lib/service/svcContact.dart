import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestlogbook/Global/Settings.dart';
import 'package:myggguestlogbook/model/CContact.dart';

class svcContact extends ChangeNotifier {

  Future<List<CContact>> getListContactBySite(String strSiteCode) async {
    var url;
    List<CContact> result = [];
    url = baseUrl + '/contact/getContactSite/' + strSiteCode;

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      //print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map contact in responseJson) {
          result.add(CContact.fromJson(contact));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

}