import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestlogbook/Global/Settings.dart';
import 'package:myggguestlogbook/model/CVisit.dart';
import 'package:myggguestlogbook/model/CVisitSaveReq.dart';
import 'package:myggguestlogbook/model/CVisitEndReq.dart';

class svcVisit extends ChangeNotifier {

  Future<CVisit> setRegisterVisit(CVisitSaveReq param) async {
    CVisit result;
    final url = baseUrl + '/visit/setsaveVisit';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    //print("setRegisterVisit -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      // print(response.body.toString());

      if (response.statusCode == 200) {
        result = CVisit.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CVisit> setEndVisit(CVisitEndReq param) async {
    CVisit result;
    final url = baseUrl + '/visit/setendVisit';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    //print("setEndVisit -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      //print(response.body.toString());

      if (response.statusCode == 200) {
        result = CVisit.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }


  Future<CVisit> getVisitById(String p_visitId) async {
    final String url = baseUrl + '/visit/getVisitbyId/' + p_visitId;
    CVisit result;
    //print(url);
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if (response.statusCode == 200) {
        final responseJson = json.decode(response.body);
       // print(response.body.toString());
        result = CVisit.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch (err) {
      result = null;
      throw err;
    }
    return result;
  }

  Future<CVisit> getVisitBySessionCode(String p_sessionCode) async {
    final String url = baseUrl + '/visit/getVisitbySessionCode/' + p_sessionCode;
    CVisit result;
    //print(url);
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if (response.statusCode == 200) {
        final responseJson = json.decode(response.body);
       // print(response.body.toString());
        result = CVisit.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch (err) {
      result = null;
      throw err;
    }
    return result;
  }

  Future<CVisit> getActiveVisitbyguestCardId(String p_guestCardId) async {
    final String url = baseUrl + '/visit/getActiveVisitbyguestCardId/' + p_guestCardId;
    CVisit result;
   // print(url);
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if (response.statusCode == 200) {
        final responseJson = json.decode(response.body);
        //print(response.body.toString());
        result = CVisit.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch (err) {
      result = null;
      throw err;
    }
    return result;
  }


}