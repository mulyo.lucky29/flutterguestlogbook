import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestlogbook/Global/Settings.dart';
import 'package:myggguestlogbook/model/CQuestionaire.dart';


class svcQuestionaire extends ChangeNotifier {

  Future<List<CQuestionaire>> getListQuestionaireById(String p_queId, String p_lang) async {
    List<CQuestionaire> result = [];
    // default indonesian else override questionaire based on language selected p_lang
    String url;
    if(p_lang == null || p_lang == "id_ID") {
      url = baseUrl + '/questionaire/getListQuestionaireById/' + p_queId;
    }
    else{
      url = baseUrl + '/questionaire/getListQuestionaireForeignById/' + p_queId + '/' + p_lang;
    }
    //print(url);

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      //print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map contact in responseJson) {
          result.add(CQuestionaire.fromJson(contact));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }
}