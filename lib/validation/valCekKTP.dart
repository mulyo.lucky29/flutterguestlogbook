
class valCekKTP {

  /*
  Sebagai contoh dari gambar di atas, 35-76-01-44-03-91-0003, cara mengetahui artinya adalah sebagai berikut:

35 : Kode Provinsi
76 : Kode Kota/Kabupaten.
01: Kode kecamatan, setiap kecamatan memiliki kode yang berbeda.
44: Tanggal lahir. Nah, di sini ada perbedaan antara kode laki-laki dan perempuan.
     Kode untuk laki-laki adalah tanggal lahir 01-31.
     sedangkan untuk perempuan berbeda lagi, tanggal lahir ditambah 40, jadinya adalah 41-71.
     Jadi kalau kamu seorang perempuan yang lahir tanggal 12 maka kodenya adalah 40 + 12 yaitu 52.
03: Mengacu pada bulan lahir, 01 untuk Januari hingga seterusnya 12 untuk Desember.
91: Tahun lahir, ditulis dua angka terakhir. Seperti jika kamu lahir tahun 1991 maka hanya ditulis 91 saja.
0003: Nomor urut pendaftaran penduduk sesuai tanggal lahir pada hari tersebut yang diproses secara otomatis oleh sistem.
*/

  bool isNumeric(String strval) {
    Pattern pattern = r'^[0-9]+$';
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(strval);
  }

  String validateNoKTP(String value) {
    //var v_nik = "3175076207200003";
    // 31 75 07 | 62 07 20 0003
    // A  B  C  | D  E  F  G


    print("NIK -> " + value);
    /*
    String v_kodeProvince = value.substring(0,2);
    String v_kodeKota = value.substring(2,4);
    String v_kodeKecamatan = value.substring(4,6);
    String v_tanggalLahir = value.substring(6,8);
    String v_bulanLahir = value.substring(8,10);
    String v_tahunLahir = value.substring(10,12);
    String v_noRegis = value.substring(12,16);

    print("v_kodeProvince  ->" + v_kodeProvince);
    print("v_kodeKota      ->" + v_kodeKota);
    print("v_kodeKecamatan ->" + v_kodeKecamatan);
    print("v_tanggalLahir  ->" + v_tanggalLahir);
    print("v_bulanLahir    ->" + v_bulanLahir);
    print("v_tahunLahir    ->" + v_tahunLahir);
    print("v_noRegis       ->" + v_noRegis);
    */

    if (value.isEmpty) {
      return 'required';
    }
    /*
    else {
      if(value.length !=16){
        return 'Nik harus 16 digit';
      }
      else{
        if(!isNumeric(value)){
          return 'Nik harus berupa angka';
        }
        else{
          return null;
          // validation tanggal lahir (D)
          // validation bulan lahir (E)
          /*
          if(int.parse(v_bulanLahir) >= 1 && int.parse(v_bulanLahir) <= 12){
            // for male
            if(int.parse(v_tanggalLahir) >= 1 && int.parse(v_tanggalLahir) <= 31) {
              return null;
            }
            else{
              // for female add 40
              //if(int.parse(v_tanggalLahir) - 40){
              return null;
              }
          }
          else{
            return 'Nik segment E Not Valid';
          }
          */
        }
      }
    }
    */
  }
}