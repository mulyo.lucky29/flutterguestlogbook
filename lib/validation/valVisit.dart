class valVisit {

  String validateContactName(String value) {
    if (value.isEmpty) {
      return 'required';
    }
    else {
      return null;
    }
  }

  String validateVisitPurpose(String value) {
    if (value.isEmpty) {
      return 'required';
    }
    else {
      return null;
    }
  }

  String validateNoDocAssurance(String value) {
    if (value.isEmpty) {
      return 'required';
    }
    else {
      return null;
    }
  }

}