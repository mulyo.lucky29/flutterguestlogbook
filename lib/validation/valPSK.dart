import 'package:hashids2/hashids2.dart';
import 'package:libcalendar/libcalendar.dart';
import 'package:myggguestlogbook/model/CSite.dart';

class valPSK {


  String validatePSK(String value, CSite objSite){
    String sharedKey;

    if(value.isEmpty){
      return 'required';
    }
    else{
      var now = DateTime.now();
      final hashids = HashIds();
      final int year = now.year ;
      final int month = now.month ;
      final int day = now.day;
      final String seed = fromGregorianToCJDN(year, month, day).toString() + objSite.siteId.toString();

      //print("PSK");
      //print(fromGregorianToCJDN(year, month, day).toString());
      //print("SiteID");
      //print(objSite.siteId.toString());
      // 2016005
      // 2459754
      sharedKey = hashids.encode(seed);

      if(value == sharedKey){
        print("OK");
        return null;
      }
      else{
        return "Key not valid ";
      }
    }
  }
}