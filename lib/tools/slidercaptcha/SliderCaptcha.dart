
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myggguestlogbook/tools/slidercaptcha/logic/slider_captcha_cubit.dart';
import 'package:myggguestlogbook/tools/slidercaptcha/presentation/slider_bar.dart';
import 'package:myggguestlogbook/tools/slidercaptcha/presentation/slider_panel.dart';

class SliderCaptcha extends StatelessWidget {
  const SliderCaptcha(
      {
        @required this.image,
        @required this.instruction,
        @required this.onSuccess,
        this.captchaSize = 30,
        Key key})
      : super(key: key);

  final Image image;
  final String instruction;

  final void Function() onSuccess;

  final double captchaSize;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocProvider(
        create: (_) => SliderCaptchaCubit(
            context: context,
            height: 150,
            sizeCaptcha: captchaSize,
            onSuccess: onSuccess),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
                height: 150,
                child: SliderPanel(sizeCaptcha: captchaSize, image: image)),
            // const SliderBar(title: 'Geser untuk meletakkan puzzle'),
            SliderBar(title: instruction),
          ],
        ),
      ),
    );
  }
}
