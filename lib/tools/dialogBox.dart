import 'package:flutter/material.dart';

class dialogBox extends ChangeNotifier {


  Future<void> showMessageDialog(
      BuildContext context,
      String strTitle,
      String strMsg
      ) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text(strTitle,
              style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w600)),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(strMsg, textAlign: TextAlign.left),
              ],
            ),
          ),
          actions: <Widget>[
            new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      child: new Container(
                        width: 200.0,
                        height: 50.0,
                        decoration: new BoxDecoration(
                          color: Colors.lightGreen,
                          border: new Border.all(color: Colors.white, width: 2.0),
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        child:
                        new Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text('Ok',
                                  style: TextStyle(fontSize: 18.0,
                                      color: Colors.black ))
                            ],
                          ),
                        ),
                      )
                  )
                ]
            ) // end column
          ],
        );
      },
    );
  }

}