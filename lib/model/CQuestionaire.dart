
class CQuestionaire {
  final int quehid;
  final int quedid;
  final int quelineno;
  final String quetext;
  final String quehname;
  final String quehdesc;
  final String queanswer;

  const CQuestionaire({
    this.quehid,
    this.quedid,
    this.quelineno,
    this.quetext,
    this.quehname,
    this.quehdesc,
    this.queanswer
  });

  factory CQuestionaire.fromJson(Map<String, dynamic> json) {
    CQuestionaire result;
    try{
      result = CQuestionaire(
          quehid: json['quehid'],
          quedid: json['quedid'],
          quelineno: json['quelineno'],
          quetext:json['quetext'],
          quehname: json['quehname'],
          quehdesc: json['quehdesc']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'quehid': quehid,
        'quedid': quedid,
        'quelineno': quelineno,
        'quetext': quetext,
        'quehname': quehname,
        'quehdesc': quehdesc,
        'queanswer': queanswer
      };
}