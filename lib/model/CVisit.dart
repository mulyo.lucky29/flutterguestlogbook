import 'package:myggguestlogbook/model/CSite.dart';
import 'package:myggguestlogbook/model/CGuest.dart';
import 'package:myggguestlogbook/model/CContact.dart';

/*
{
  "visitId": 73,
  "visitSessionCode": "9097cfe4b1abede2655087afea64516f",
  "visitDate": "2021-10-02",
  "visitPurpose": "asdf",
  "checkIn": "2021-10-02 10:08:50",
  "checkOut": "2021-10-02 10:08:57",
  "endVisitDate": "2021-10-02 10:09:15",
  "endVisitFlag": "Y",
  "endVisitReason": "",
  "endVisitBy": "Lucky Mulyo",
  "objGuest": {
    "guestId": 1,
    "guestCardId": "12345",
    "guestName": "Lucky Mulyo",
    "guestPhone": "081310267800",
    "guestEmail": "lucky.mulyo@gudanggaramtbk.com",
    "guestCompany": "Gudang Garam",
    "guestPictureUrl": "",
    "guestPictureCardId": "",
    "isActive": null
  },
  "objSite": {
    "siteId": 4,
    "siteCode": "JKP_GG_Tower",
    "siteName": "GG Tower",
    "isActive": null
  },
  "objContact": {
    "contactId": 1525,
    "siteCode": "JKP_GG_Tower",
    "siteSpecific": "Basement",
    "tenantCompany": null,
    "contactType": null,
    "contactName": "Lisan Arisandy",
    "contactPhone": null,
    "contactEmail": null,
    "contactExt": "800202",
    "contactAltName": null
  },
  "assuranceCardType": null,
  "assuranceCardNo": null,
  "accessCardTag": null,
  "accessCardPairNo": null
}
 */

class CVisit {
  final int visitId;
  final String visitSessionCode;
  final String visitDate;
  final String visitPurpose;
  final String checkIn;
  final String checkOut;
  final CSite objSite;
  final CGuest objGuest;
  final CContact objContact;
  final String endVisitDate;
  final String endVisitFlag;
  final String endVisitReason;
  final String endVisitBy;
  final String assuranceCardType;
  final String assuranceCardNo;
  final String accessCardTag;
  final String accessCardPairNo;


  const CVisit({
    this.visitId,
    this.visitSessionCode,
    this.visitDate,
    this.visitPurpose,
    this.checkIn,
    this.checkOut,
    this.objSite,
    this.objGuest,
    this.objContact,
    this.endVisitDate,
    this.endVisitFlag,
    this.endVisitReason,
    this.endVisitBy,
    this.assuranceCardType,
    this.assuranceCardNo,
    this.accessCardTag,
    this.accessCardPairNo
  });

  factory CVisit.fromJson(Map<String, dynamic> json) {
    CVisit result;
    try{
      result = CVisit(
          visitId: json['visitId'],
          visitSessionCode: json['visitSessionCode'],
          visitDate: json['visitDate'],
          visitPurpose:json['visitPurpose'],
          checkIn:json['checkIn'],
          checkOut:json['checkOut'],
          objSite: CSite.fromJson(json['objSite']),
          objGuest: CGuest.fromJson(json['objGuest']),
          objContact: CContact.fromJson(json['objContact']),
          endVisitDate: json['endVisitDate'],
          endVisitFlag: json['endVisitFlag'],
          endVisitReason: json['endVisitReason'],
          endVisitBy: json['endVisitBy'],
          assuranceCardType: json['assuranceCardType'],
          assuranceCardNo: json['assuranceCardNo'],
          accessCardTag: json['accessCardTag'],
          accessCardPairNo: json['accessCardPairNo']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'visitId': visitId,
        'visitSessionCode': visitSessionCode,
        'visitDate': visitDate,
        'visitPurpose': visitPurpose,
        'checkIn': checkIn,
        'checkOut': checkOut,
        'objSite': objSite,
        'objGuest': objGuest,
        'objContact': objContact,
        'endVisitDate': endVisitDate,
        'endVisitFlag': endVisitFlag,
        'endVisitReason': endVisitReason,
        'endVisitBy': endVisitBy,
        'assuranceCardType': assuranceCardType,
        'assuranceCardNo': assuranceCardNo,
        'accessCardTag': accessCardTag,
        'accessCardPairNo': accessCardPairNo
      };

}