/*
{
  "siteId": 8,
  "siteCode": "JKP_GG_Tower",
  "siteName": "GG Tower",
  "isActive": "Y",
  "formaccessmentid": 1
}
 */

class CSite {
  final int siteId;
  final String siteCode;
  final String siteName;
  final String isActive;
  final int formaccessmentid;

  const CSite({
    this.siteId,
    this.siteCode,
    this.siteName,
    this.isActive,
    this.formaccessmentid
  });

  factory CSite.fromJson(Map<String, dynamic> json) {
    CSite result;
    try{
      result = CSite(
          siteId: json['siteId'],
          siteCode: json['siteCode'],
          siteName: json['siteName'],
          isActive:json['isActive'],
          formaccessmentid: json['formaccessmentid']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'siteId': siteId,
        'siteCode': siteCode,
        'siteName': siteName,
        'isActive': isActive,
        'formaccessmentid': formaccessmentid
      };


}