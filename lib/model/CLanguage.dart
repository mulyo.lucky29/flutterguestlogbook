
class CLanguage {
  final String langCode;
  final String langName;

  const CLanguage({
    this.langCode,
    this.langName
  });

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'langCode': langCode,
        'langName': langName
      };
}