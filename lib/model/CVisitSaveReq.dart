

/*
{
  "siteid": 0,
  "guestid": 0,
  "contactid": 0,
  "visitpurpose": "string",
  "contactaltname": "string",
  "assuranceCardType": "string",
  "assuranceCardNo": "string"
}

 */

class CVisitSaveReq {
  final int siteid;
  final int guestid;
  final int contactid;
  final int visitid;
  final String visitpurpose;
  final String contactaltname;
  final String assuranceCardType;
  final String assuranceCardNo;
  final String formaccessment;

  const CVisitSaveReq({
    this.siteid,
    this.guestid,
    this.contactid,
    this.visitid,
    this.visitpurpose,
    this.contactaltname,
    this.assuranceCardType,
    this.assuranceCardNo,
    this.formaccessment
  });

  factory CVisitSaveReq.fromJson(Map<String, dynamic> json) {
    CVisitSaveReq result;
    try{
      result = CVisitSaveReq(
          siteid: json['siteid'],
          guestid: json['guestid'],
          contactid: json['contactid'],
          visitpurpose: json['visitpurpose'],
          contactaltname: json['contactaltname'],
          assuranceCardType: json['assuranceCardType'],
          assuranceCardNo: json['assuranceCardNo'],
          formaccessment: json['formaccessment']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'siteid': siteid,
        'guestid': guestid,
        'contactid': contactid,
        'visitpurpose': visitpurpose,
        'contactaltname': contactaltname,
        'assuranceCardType': assuranceCardType,
        'assuranceCardNo': assuranceCardNo,
        'formaccessment': formaccessment
      };

}