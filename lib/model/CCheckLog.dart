
/*
{
  "checkLogId": 15,
  "visitId": 42,
  "visitSessionCode": "59ba00820fbeb13ccfa9498e95153b9f",
  "checkedTime": "2021-09-21 18:02:04",
  "checkTyped": "CHECK-OUT",
  "checkedTerminalId": "Terminal Velocity"
}
 */

class CCheckLog {
  final int checkLogId;
  final int visitId;
  final String visitSessionCode;
  final String checkedTime;
  final String checkTyped;
  final String checkedTerminalId;


  const CCheckLog({
    this.checkLogId,
    this.visitId,
    this.visitSessionCode,
    this.checkedTime,
    this.checkTyped,
    this.checkedTerminalId
  });

  factory CCheckLog.fromJson(Map<String, dynamic> json) {
    CCheckLog result;
    try{
      result = CCheckLog(
          checkLogId: json['checkLogId'],
          visitId: json['visitId'],
          visitSessionCode: json['visitSessionCode'],
          checkedTime:json['checkedTime'],
          checkTyped: json['checkTyped'],
          checkedTerminalId: json['checkedTerminalId']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'checkLogId': checkLogId,
        'visitId': visitId,
        'visitSessionCode': visitSessionCode,
        'checkedTime': checkedTime,
        'checkTyped': checkTyped,
        'checkedTerminalId': checkedTerminalId
      };


}