
class CVisitDocAssurance {
  final int docIndex;
  final String docType;
  final String docDesc;
  final String docNo;

  const CVisitDocAssurance({
    this.docIndex,
    this.docType,
    this.docDesc,
    this.docNo
  });

}
