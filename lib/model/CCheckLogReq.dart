
class CCheckLogReq {
  final String visitSessionCode;
  final String terminalId;

  const CCheckLogReq({
    this.visitSessionCode,
    this.terminalId
  });

  factory CCheckLogReq.fromJson(Map<String, dynamic> json) {
    CCheckLogReq result;
    try{
      result = CCheckLogReq(
          visitSessionCode: json['visitSessionCode'],
          terminalId: json['terminalId']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'visitSessionCode': visitSessionCode,
        'terminalId': terminalId
      };
}
