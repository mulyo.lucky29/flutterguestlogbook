import 'package:flutter/material.dart';
import 'package:myggguestlogbook/screen/SplashPage.dart';
import 'package:myggguestlogbook/screen/NotFoundPage.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load();
  await EasyLocalization.ensureInitialized();

  //runApp(MyApp());
  runApp(
      EasyLocalization(
          supportedLocales: [
            Locale('id', 'ID'),
            Locale('en', 'US')
          ],
          path: 'assets/translations', // <-- change the path of the translation files
          fallbackLocale: Locale('id', 'ID'),
          child: MyApp()
      ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //context.setLocale(Locale('en', 'US'));
    //get language selected
    //print(context.locale.toString());

    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'GG Guest Log Book',
      theme: ThemeData(
          primaryColor: Colors.blue,
          accentColor: Colors.pink,
          canvasColor: Color.fromRGBO(255, 255, 255, 2), //WARNA BACKGROUND CANVASNYA PAKAI RGBO DIMANA CODE DIATAS AKAN MENGHASILKAN WARNA KUNING
          textTheme: ThemeData.light().textTheme.copyWith(
            title: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), //TITLE KITA GUNAKAN BOLD DAN SIZE 20
            subhead: TextStyle(fontWeight: FontWeight.bold), //SUBHEADNYA CUKUP BOLD SAJA
          )
      ),
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          builder: (BuildContext context) => makeRoute(
            context: context,
            routeName: settings.name,
            arguments: settings.arguments,
          ),
          maintainState: true,
          fullscreenDialog: false,
        );
      },
      /*
      routes: {
        '/'         : (ctx) => splashPage(siteparam: siteSelect),
      },
      */
    );
  }

  Widget makeRoute(
      {@required BuildContext context,
        @required String routeName,
        Object arguments})
  {
    final Widget child = _buildRoute(context: context, routeName: routeName, arguments: arguments);
    return child;
  }

  Widget _buildRoute({
    @required BuildContext context,
    @required String routeName,
    Object arguments,
  })
  {
    //print('routeName -> ' + routeName);
    if(routeName == "/"){
      //print('route without parameter');
      return NotFoundPage();
      //SplashPage(siteparam: "JKP_GG_Lama");
    }
    else{
      //print('route with parameter');
      if(routeName.contains("/")){
        return SplashPage(siteparam: routeName.replaceAll("/", ""));
      }
    }
  }
}

